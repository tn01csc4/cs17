﻿using RESTC.UserDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace RESTC.SystemCorsDirectory
{
    public class Corsdetails
    {
        public int id { get; set; }
        public String Origin { get; set; }
        public String Description { get; set; }
        public String Header { get; set; }
        public String ExposedHeader { get; set; }
        public Boolean _GET { get; set; }
        public Boolean _POST { get; set; }
        public Boolean _DELETE { get; set; }
        public Boolean _PUT { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime createddate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Lastmodifieddate { get; set; }
        [Required]
        public virtual User user { get; set; }
        [Required]
        public Boolean Enabled { get; set; }
        public String getMethods()
        {
            if (_GET && _PUT && _POST && _DELETE)
            {
                return "*";
            }
            else
            {
                String Methods = "";
                if (_GET)
                {
                    Methods += "," + "GET";
                }
                if (_POST)
                {
                    Methods += "," + "Post";
                }
                if (_PUT)
                {
                    Methods += "," + "PUT";
                }
                if (_DELETE)
                {
                    Methods += "," + "Delete";
                }
                return Methods.Substring(1);
            }
        }
    }
}
