﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.SystemCorsDirectory
{
    public class Cors_Implementation : IDAO<Corsdetails>
    {
        private Hrmodel db = new Hrmodel();
        public Boolean delete(Corsdetails cors)
        {
            try
            {
                if (cors != null)
                {
                    db.cors.Remove(cors);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean insert(Corsdetails cors)
        {
            try
            {
                if (cors != null)
                {
                    db.cors.Attach(cors);
                    db.Entry(cors).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Corsdetails select(int id)
        {
            try
            {

                return db.cors.Find(id);

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Corsdetails> select()
        {
            try
            {
                return db.cors.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean update(Corsdetails cors)
        {
            try
            {
                if (cors != null)
                {
                    db.cors.Attach(cors);
                    db.Entry(cors).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
    }
}
