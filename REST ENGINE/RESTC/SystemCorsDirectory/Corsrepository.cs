﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.SystemCorsDirectory
{
    public class Corsrepository
    {
        private IDAO<Corsdetails> dao = new Cors_Implementation();
        public Boolean Add_Cors_config(Corsdetails cor)
        {
            try
            {
                if (cor != null)
                {
                    cor.createddate = DateTime.Now;
                    cor.Lastmodifieddate = DateTime.Now;
                    return dao.insert(cor);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Remove_Cors_config(Corsdetails cor)
        {
            try
            {
                if (cor != null)
                {
                    return dao.delete(cor);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Modify_Cors_config(Corsdetails cor)
        {
            try
            {
                if (cor != null)
                {
                    cor.Lastmodifieddate = DateTime.Now;
                    return dao.update(cor);

                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Corsdetails Get_Cors_config_details(int id)
        {
            try
            {
                return dao.select(id);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<Corsdetails> Get_All_Cors_config_details()
        {
            try
            {
                return dao.select();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
