﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public class Table_fields_constraints
    {
        public virtual Db_Table_Field Field { get; set; }
        public List<Constraints> constraints { get; set; }
        public PrimarykeyConstraint Primarykey { get; set; }
        public NotnullConstraint Notnull { get; set; }
        public DefaultConstraint Default { get; set; }
        public ForiegnkeyConstraint foriegnkey { get; set; }
        public UniqueConstraint Unique { get; set; }
    }
}
