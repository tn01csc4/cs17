﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public class UniqueConstraint : Constraints
    {
        public UniqueConstraint()
        {
            this.Name = "Unique";
        }

        public override string get_Constraint_query_()
        {
            return Name;
        }
    }
}
