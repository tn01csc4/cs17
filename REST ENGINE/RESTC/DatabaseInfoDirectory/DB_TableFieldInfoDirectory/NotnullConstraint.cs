﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public class NotnullConstraint : Constraints
    {
        public NotnullConstraint()
        {
            this.Name = "NOT NULL";
        }

        public override string get_Constraint_query_()
        {
            return Name;
        }
    }
}
