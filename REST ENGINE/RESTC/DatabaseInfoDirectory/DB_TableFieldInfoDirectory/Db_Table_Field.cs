﻿using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public class Db_Table_Field
    {
        public int id { get; set; }
        public int Table_id { get; set; }
        [NotMapped]
        public virtual Db_tables Table { get; set; }
        public String Name { get; set; }
        public String PropertyType { get; set; }
        public int Length { get; set; }



        [Column(TypeName = "datetime2")]
        public DateTime created_date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime last_mofified_at { get; set; }
    }
}
