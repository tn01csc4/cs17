﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public class Db_tableField_Implementation : IDAO<Table_fields_constraints>
    {
        private Hrmodel db = new Hrmodel();
        public Boolean delete(Table_fields_constraints t)
        {
            try
            {
                if (t != null)
                {
                    foreach (Constraints item in t.constraints)
                    {
                        var a = db.constraints.Find(item.id);
                        db.constraints.Remove(a);

                    }
                    db.db_Table_Fields.Remove(db.db_Table_Fields.Find(t.Field.id));
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean insert(Table_fields_constraints t)
        {
            try
            {
                if (t != null)
                {
                    db.db_Table_Fields.Add(t.Field);

                    foreach (Constraints cons in t.constraints)
                    {
                        cons.Field = t.Field;
                        db.constraints.Attach(cons);
                        db.Entry(cons).State = System.Data.Entity.EntityState.Added;
                    }
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Table_fields_constraints select(int id)
        {
            try
            {
                Table_fields_constraints x = new Table_fields_constraints();
                x.Field = db.db_Table_Fields.Find(id);
                x.constraints = db.constraints.ToList().Where(y => y.Field.id == x.Field.id).ToList();
                foreach (Constraints cs in x.constraints)
                {
                    if (cs.Name == "Primary key")
                    {
                        x.Primarykey = (PrimarykeyConstraint)cs;
                    }
                    if (cs.Name == "Unique")
                    {
                        x.Unique = (UniqueConstraint)cs;

                    }
                    if (cs.Name == "NOT NULL")
                    {
                        x.Notnull = (NotnullConstraint)cs;

                    }
                    if (cs.Name == "Default")
                    {
                        x.Default = (DefaultConstraint)cs;
                    }
                    if (cs.Name == "Foreign key")
                    {
                        x.foriegnkey = (ForiegnkeyConstraint)cs;

                    }
                }
                return x;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Table_fields_constraints> select()
        {
            try
            {
                List<Table_fields_constraints> tfc = new List<Table_fields_constraints>();
                List<Db_Table_Field> fields = db.db_Table_Fields.ToList();
                foreach (Db_Table_Field field in fields)
                {
                    Table_fields_constraints table_Field_ = new Table_fields_constraints();
                    table_Field_.Field = field;

                    List<Constraints> constraints = db.constraints.ToList();
                    table_Field_.constraints = (from Constraint in constraints where Constraint.Field == field select Constraint).ToList();
                    foreach (Constraints cs in table_Field_.constraints)
                    {
                        if (cs.Name == "Primary key")
                        {
                            table_Field_.Primarykey = (PrimarykeyConstraint)cs;
                        }
                        if (cs.Name == "Unique")
                        {
                            table_Field_.Unique = (UniqueConstraint)cs;

                        }
                        if (cs.Name == "NOT NULL")
                        {
                            table_Field_.Notnull = (NotnullConstraint)cs;

                        }
                        if (cs.Name == "Default")
                        {
                            table_Field_.Default = (DefaultConstraint)cs;
                        }
                        if (cs.Name == "Foreign key")
                        {
                            table_Field_.foriegnkey = (ForiegnkeyConstraint)cs;

                        }
                    }
                    tfc.Add(table_Field_);
                }
                return tfc;

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean update(Table_fields_constraints t)
        {
            try
            {
                if (t != null)
                {
                    db.db_Table_Fields.Attach(t.Field);
                    db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
    }
}
