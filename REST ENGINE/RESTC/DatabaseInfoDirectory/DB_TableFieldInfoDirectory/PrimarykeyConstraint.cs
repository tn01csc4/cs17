﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public class PrimarykeyConstraint : Constraints
    {
        public PrimarykeyConstraint()
        {
            this.Name = "Primary key";
        }

        public override string get_Constraint_query_()
        {
            return Name + " (" + Field.Name + ")";
        }
    }
}
