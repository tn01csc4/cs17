﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public class DefaultConstraint : Constraints
    {
        public String defaultvalue { get; set; }
        public DefaultConstraint()
        {
            this.Name = "Default";
        }

        public override string get_Constraint_query_()
        {
            String query = Name;
            query += Field.PropertyType == "DATETIME" || Field.PropertyType == "DATETIME2" || Field.PropertyType == "DATE" || Field.PropertyType == "TIME" ? " getdate()" : " '" + defaultvalue + "'";
            return query;
        }
    }
}
