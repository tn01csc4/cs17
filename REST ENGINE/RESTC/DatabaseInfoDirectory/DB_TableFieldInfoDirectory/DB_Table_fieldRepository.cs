﻿using RESTC.HelperDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public class DB_Table_fieldRepository
    {
        Hrmodel db = new Hrmodel();
        Dynamic_DDL_Commands ddl = new Dynamic_DDL_Commands();
        DB_TableInfoDirectory.Db_tableRepository repo = new DB_TableInfoDirectory.Db_tableRepository();
        private Db_tableField_Implementation dao = new Db_tableField_Implementation();
        public Boolean Add_Table_Field(List<Table_fields_constraints> db_Table_Fields)
        {
            try
            {
                if (db_Table_Fields != null)
                {

                    if (repo.Add_Db_Table_Record(db_Table_Fields[0].Field.Table))
                    {
                        List<Table_fields_constraints> newlist = new List<Table_fields_constraints>();
                        foreach (Table_fields_constraints db_Table_Field in db_Table_Fields)
                        {
                            db_Table_Field.Field.Table_id = db_Table_Fields[0].Field.Table.id;
                            db_Table_Field.Field.created_date = DateTime.Now;
                            db_Table_Field.Field.last_mofified_at = DateTime.Now;
                            db_Table_Field.constraints = new List<Constraints>();
                            if (db_Table_Field.Primarykey != null)
                            {
                                db_Table_Field.constraints.Add(db_Table_Field.Primarykey);
                            }
                            else if (db_Table_Field.foriegnkey != null)
                            {
                                db_Table_Field.constraints.Add(db_Table_Field.foriegnkey);
                            }
                            else if (db_Table_Field.Default != null)
                            {
                                db_Table_Field.constraints.Add(db_Table_Field.Default);
                            }
                            else
                            {
                                if (db_Table_Field.Notnull != null)
                                {
                                    db_Table_Field.constraints.Add(db_Table_Field.Notnull);
                                }
                                if (db_Table_Field.Unique != null)
                                {
                                    db_Table_Field.constraints.Add(db_Table_Field.Unique);
                                }
                            }
                            if (dao.insert(db_Table_Field))
                            {
                                newlist.Add(db_Table_Field);
                            }
                            else
                            {
                                repo.Remove_Db_Table_Record(repo.Get_Table_Record(db_Table_Field.Field.Table_id));
                                foreach (Table_fields_constraints newcons in newlist)
                                {
                                    Remove_Table_Field(newcons);
                                }
                                return false;
                            }
                        }
                        if (ddl.Create_table(db_Table_Fields) == "Successfully created")
                        {
                            return true;
                        }
                        repo.Remove_Db_Table_Record(repo.Get_Table_Record(db_Table_Fields[0].Field.Table_id));
                        foreach (Table_fields_constraints newcons in newlist)
                        {
                            dao.delete(newcons);
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public void Modify_Table_Field(Table_fields_constraints db_Table_Field)
        {
            try
            {
                if (db_Table_Field != null)
                {
                    dao.update(db_Table_Field);

                }
            }
            catch (Exception e)
            {

            }
        }
        public void Remove_Table_Field(Table_fields_constraints db_Table_Field)
        {
            try
            {
                if (db_Table_Field != null)
                {
                    dao.delete(db_Table_Field);
                }
            }
            catch (Exception e)
            {
            }
        }
        public Table_fields_constraints Get_Table_Field_details(int id)
        {
            try
            {
                return dao.select(id);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<Table_fields_constraints> Get_All_Table_Field_details(int table_id)
        {
            try
            {
                List<Table_fields_constraints> fields = dao.select();
                foreach (Table_fields_constraints table_Fields_ in fields)
                {
                    table_Fields_.Field.Table = repo.Get_Table_Record(table_Fields_.Field.Table_id);
                }
                return (from Table_fields_constraints in fields where Table_fields_constraints.Field.Table_id == table_id select Table_fields_constraints).ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public String dropColumn(int id)
        {
            try
            {
                Table_fields_constraints table_Field = Get_Table_Field_details(id);
                if (table_Field != null)
                {
                    foreach (Constraints item in table_Field.constraints)
                    {
                        if (item.Name == "Primary key" || item.Name == "Default" || item.Name == "Foreign key" || item.Name == "Unique")
                        {
                            return "failed to Drop column because one or more objects access this column";
                        }
                    }
                    Hrmodel db = new Hrmodel();
                    foreach (Constraints item in table_Field.constraints)
                    {
                        db.constraints.Remove(db.constraints.Find(item.id));
                    }
                    db.db_Table_Fields.Remove(db.db_Table_Fields.Find(table_Field.Field.id));
                    Dynamic_DDL_Commands ddl = new Dynamic_DDL_Commands();
                    String result = ddl.DropColumn(table_Field.Field);
                    if (result == "Column Droped")
                    {
                        db.SaveChanges();
                        return result;
                    }
                    return result;
                }
                return "Field Not Found";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public String AddColumn(Table_fields_constraints table_Fields_)
        {
            try
            {
                if (table_Fields_ != null)
                {
                    table_Fields_.constraints = new List<Constraints>();
                    if (table_Fields_.Primarykey != null)
                    {
                        table_Fields_.constraints.Add(table_Fields_.Primarykey);
                    }
                    else if (table_Fields_.foriegnkey != null)
                    {
                        table_Fields_.constraints.Add(table_Fields_.foriegnkey);
                    }
                    else if (table_Fields_.Default != null)
                    {
                        table_Fields_.constraints.Add(table_Fields_.Default);
                    }
                    else
                    {
                        if (table_Fields_.Notnull != null)
                        {
                            table_Fields_.constraints.Add(table_Fields_.Notnull);
                        }
                        if (table_Fields_.Unique != null)
                        {
                            table_Fields_.constraints.Add(table_Fields_.Unique);
                        }
                    }
                    table_Fields_.Field.created_date = DateTime.Now;
                    table_Fields_.Field.last_mofified_at = DateTime.Now;
                    db.db_Table_Fields.Add(table_Fields_.Field);
                    foreach (Constraints c in table_Fields_.constraints)
                    {
                        c.Field = table_Fields_.Field;
                        db.constraints.Attach(c);
                        db.Entry(c).State = System.Data.Entity.EntityState.Added;

                    }
                    String result = ddl.AddColumn(table_Fields_);
                    if (result == "Column Added")
                    {
                        db.SaveChanges();
                        return result;
                    }
                    return result;

                }
                return "Provide A Column";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
