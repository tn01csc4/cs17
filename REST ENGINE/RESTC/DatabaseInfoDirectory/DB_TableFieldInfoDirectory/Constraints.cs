﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public abstract class Constraints
    {
        public int id { get; set; }
        public String Name { get; set; }
        public virtual Db_Table_Field Field { get; set; }
        public abstract String get_Constraint_query_();
    }
}
