﻿using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory
{
    public class ForiegnkeyConstraint : Constraints
    {
        [Required]
        public int table_id { get; set; }
        public ForiegnkeyConstraint()
        {
            this.Name = "Foreign key";
        }

        public override string get_Constraint_query_()
        {
            Hrmodel hrmodel = new Hrmodel();
            Db_tableRepository tableRepository = new Db_tableRepository();
            Db_tables tablename = tableRepository.Get_Table_Record(table_id);
            Db_tables table = tableRepository.Get_Table_Record(Field.Table_id);
            String query = "Constraint FK_" + tablename.Name + table.Name + " " + Name + " (" + Field.Name + ") References " + tablename.Name + "(";
            List<Constraints> constraints = hrmodel.constraints.ToList();
            var query1 = from Constraint in constraints where Constraint.Field.Table_id == tablename.id && Constraint.Name == "Primary key" select Constraint;
            if (query1.Count() == 1)
            {
                Constraints cns = query1.FirstOrDefault();
                query += cns.Field.Name + ")";
            }
            return query;
        }
    }
}
