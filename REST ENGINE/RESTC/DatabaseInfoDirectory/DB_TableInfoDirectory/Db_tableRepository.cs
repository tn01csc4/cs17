﻿using RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory
{
    public class Db_tableRepository
    {

        private ServiceDirectory.ServiceRepository repo = new ServiceDirectory.ServiceRepository();
        private Db_tablesImplementation dao = new Db_tablesImplementation();
        public Boolean Add_Db_Table_Record(Db_tables db_Tables)
        {
            try
            {
                if (db_Tables != null)
                {
                    db_Tables.created_date = DateTime.Now;
                    db_Tables.last_mofified_at = DateTime.Now;
                    return dao.insert(db_Tables);
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public Boolean Remove_Db_Table_Record(Db_tables db_Tables)
        {
            try
            {

                if (db_Tables != null)
                {
                    Db_tableField_Implementation dao1 = new Db_tableField_Implementation();
                    DB_Table_fieldRepository trepo = new DB_Table_fieldRepository();
                    List<Table_fields_constraints> tfc = trepo.Get_All_Table_Field_details(db_Tables.id);
                    if (dao.delete(db_Tables))
                    {
                        foreach (Table_fields_constraints item in tfc)
                        {
                            dao1.delete(item);
                        }
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public Boolean Modify_Db_Table_Record(Db_tables db_Tables)
        {
            try
            {
                if (db_Tables != null)
                {
                    return dao.update(db_Tables);
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public Db_tables Get_Table_Record(int id)
        {
            try
            {
                Db_tables table = dao.select(id);
                table.Service = repo.Get_Service_details(table.service_id);
                return table;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<Db_tables> Get_All_Table_Record()
        {
            try
            {
                List<Db_tables> tables_ = dao.select();
                foreach (Db_tables table in tables_)
                {
                    table.Service = repo.Get_Service_details(table.service_id);
                }
                return tables_;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        public List<Db_tables> Get_tables_(int service_id)
        {
            try
            {
                List<Db_tables> tables = Get_All_Table_Record();
                var query = from Db_tables in tables where Db_tables.service_id == service_id select Db_tables;
                List<Db_tables> tables_ = query.ToList();
                foreach (Db_tables table in tables_)
                {
                    table.Service = repo.Get_Service_details(table.service_id);
                }
                return tables_;

            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public List<Db_tables> get_Tables_for_PK(int service__id)
        {
            try
            {
                Hrmodel db = new Hrmodel();
                List<Db_tables> Resulttables = new List<Db_tables>();
                List<Db_tables> tables = Get_tables_(service__id);
                List<Constraints> constraints = db.constraints.ToList();
                foreach (Db_tables table in tables)
                {
                    var query = from Constraint in constraints where Constraint.Name == "Primary key" && Constraint.Field.Table_id == table.id select Constraint;
                    if (query.Count() == 1)
                    {
                        Resulttables.Add(table);
                    }
                }
                return Resulttables;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
