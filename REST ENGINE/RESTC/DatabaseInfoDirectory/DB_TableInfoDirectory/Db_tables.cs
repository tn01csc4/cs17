﻿using RESTC.ServiceDirectory;
using RESTC.UserDirectory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory
{
    public class Db_tables
    {
        public int id { get; set; }
        public String Name { get; set; }
        public String Label { get; set; }
        public String Description { get; set; }
        [Required]
        public virtual User user { get; set; }

        public int service_id { get; set; }
        [NotMapped]
        public virtual Service Service { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime created_date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime last_mofified_at { get; set; }
    }
}
