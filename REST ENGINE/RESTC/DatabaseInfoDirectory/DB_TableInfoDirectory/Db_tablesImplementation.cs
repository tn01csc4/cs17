﻿using RESTC.HelperDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory
{
    public class Db_tablesImplementation : IDAO<Db_tables>
    {
        private Hrmodel db = new Hrmodel();

        public Boolean delete(Db_tables t)
        {
            try
            {

                if (t != null)
                {
                    Dynamic_DDL_Commands ddl = new Dynamic_DDL_Commands();
                    db.db_Tables.Remove(t);
                    String result = ddl.DropTable(t);
                    if (result == "Successfully Droped" || result == "Cannot drop the table '" + t.Name + "', because it does not exist or you do not have permission.")
                    {
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean insert(Db_tables t)
        {
            try
            {
                if (t != null)
                {
                    db.db_Tables.Attach(t);
                    db.Entry(t).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Db_tables select(int id)
        {
            try
            {
                return db.db_Tables.Find(id);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Db_tables> select()
        {
            try
            {
                return db.db_Tables.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean update(Db_tables t)
        {
            try
            {
                if (t != null)
                {
                    Db_tables table = select(t.id);

                    table.Label = t.Label;
                    table.Description = t.Description;
                    table.last_mofified_at = DateTime.Now;
                    if (table.Name == t.Name)
                    {
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        String oldname = table.Name;
                        table.Name = t.Name;
                        Dynamic_DDL_Commands ddl = new Dynamic_DDL_Commands();
                        if (ddl.RenameTable(oldname, table) == "Successfully Renamed")
                        {
                            db.SaveChanges();
                            return true;
                        }
                        return false;
                    }

                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}
