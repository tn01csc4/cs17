﻿using RESTC.Email_templateDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace RESTC
{
    public class EmailSender
    {

        public Boolean Send_Mail(Email_template template)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress(template.Senders_Email);
                message.To.Add(new MailAddress(template.Reciever_Email));
                message.Subject = template.Subject;
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = template.Body;
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com"; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = new NetworkCredential(template.Senders_Email, "Sanket@126");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
        }
    }
}
