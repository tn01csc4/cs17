﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.Email_templateDirectory
{
    public class Emailrepository
    {
        private Email_implementsion dao = new Email_implementsion();
        public Boolean Add_Email_Template(Email_template email_Template)
        {
            try
            {
                if (email_Template != null)
                {
                    email_Template.isdeletable = true;
                    email_Template.Modifieddate = DateTime.Now;
                    email_Template.Createddate = DateTime.Now;
                    return dao.insert(email_Template);
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Remove_Email_Template(Email_template email_Template)
        {
            try
            {
                if (email_Template != null)
                {
                    if (email_Template.isdeletable)
                    {
                        return dao.delete(email_Template);
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Modify_Email_Template(Email_template email_Template)
        {
            try
            {
                if (email_Template != null)
                {
                    email_Template.Modifieddate = DateTime.Now;
                    return dao.update(email_Template);
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Email_template Get_Email_Template(int id)
        {
            try
            {
                return dao.select(id);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<Email_template> Get_All_Email_Templates()
        {
            try
            {
                return dao.select();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
