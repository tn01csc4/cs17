﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.Email_templateDirectory
{
    public class Email_implementsion : IDAO<Email_template>
    {
        private Hrmodel db = new Hrmodel();

        public Boolean delete(Email_template t)
        {
            try
            {
                if (t != null)
                {
                    if (t.isdeletable)
                    {
                        db.email_Templates.Remove(t);
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean insert(Email_template t)
        {
            try
            {
                if (t != null)
                {
                    db.email_Templates.Attach(t);
                    db.Entry(t).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Email_template select(int id)
        {
            try
            {
                return db.email_Templates.Find(id);
            }
            catch (Exception e)
            {
                return null;

            }
        }

        public List<Email_template> select()
        {
            try
            {
                return db.email_Templates.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean update(Email_template t)
        {
            try
            {
                if (t != null)
                {
                    db.email_Templates.Attach(t);
                    db.Entry(t).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
    }
}
