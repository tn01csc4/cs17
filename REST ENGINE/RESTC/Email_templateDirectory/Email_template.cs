﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RESTC.UserDirectory;

namespace RESTC.Email_templateDirectory
{
    public class Email_template
    {
        public int id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Subject { get; set; }
        [EmailAddress(ErrorMessage = "Enter a valid Email address")]
        public String Reciever_Email { get; set; }
        [EmailAddress]
        public String Senders_Email { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Createddate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Modifieddate { get; set; }
        public String Body { get; set; }
        public String SendersName { get; set; }
        public virtual User User { get; set; }
        public Boolean isdeletable { get; set; }

    }
}
