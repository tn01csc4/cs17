﻿using RESTC.UserDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC
{
    public class UserAuthentication
    {
        public int id { get; set; }
        public virtual User user { get; set; }
    }
}
