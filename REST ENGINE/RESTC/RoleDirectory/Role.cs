﻿using RESTC.UserDirectory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.RoleDirectory
{
    public class Role
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Provide app Name")]
        public String Name { get; set; }
        public String Description { get; set; }
        public Boolean isactive { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime created_date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Lastmodifydate { get; set; }
        public Boolean _GET { get; set; }
        public Boolean _POST { get; set; }
        public Boolean _PUT { get; set; }
        public Boolean _DELETE { get; set; }
        public virtual User user { get; set; }
    }
}
