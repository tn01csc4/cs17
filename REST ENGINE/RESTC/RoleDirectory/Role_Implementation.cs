﻿using RESTC.ServiceRoleDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.RoleDirectory
{
    public class Role_Implementation : IDAO<Role>
    {
        private Hrmodel db = new Hrmodel();
        public Boolean delete(Role role)
        {
            try
            {
                if (role != null)
                {
                    List<ServiceRole> serviceRoles = db.serviceRoles.ToList();
                    var query = from ServiceRole in serviceRoles where ServiceRole.rol_id == role.id select ServiceRole;
                    if (query.Count() == 0)
                    {
                        db.roles.Remove(role);
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean insert(Role role)
        {
            try
            {
                if (role != null)
                {
                    db.roles.Attach(role);
                    db.Entry(role).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Role select(int id)
        {
            try
            {
                return db.roles.Find(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return null;
            }
        }

        public List<Role> select()
        {
            try
            {
                return db.roles.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return null;
            }
        }

        public Boolean update(Role role)
        {
            try
            {
                db.roles.Attach(role);
                db.Entry(role).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
    }
}
