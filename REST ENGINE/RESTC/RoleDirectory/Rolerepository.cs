﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.RoleDirectory
{
    public class Rolerepository
    {
        private IDAO<Role> dao = new Role_Implementation();
        public Boolean Set_Role(Role role)
        {
            try
            {
                if (role != null)
                {
                    role.created_date = DateTime.Now;
                    role.Lastmodifydate = DateTime.Now;
                    return dao.insert(role);
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Modify_Role(Role role)
        {
            try
            {
                if (role != null)
                {
                    return dao.update(role);
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Remove_Role(Role role)
        {
            try
            {
                if (role != null)
                {
                    return dao.delete(role);
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Role Get_Role(int id)
        {
            try
            {
                return dao.select(id);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<Role> Get_All_Roles()
        {
            try
            {
                return dao.select();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
