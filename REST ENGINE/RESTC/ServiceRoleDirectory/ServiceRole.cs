﻿using RESTC.RoleDirectory;
using RESTC.ServiceDirectory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.ServiceRoleDirectory
{
    public class ServiceRole
    {
        public int id { get; set; }
        [NotMapped]
        public virtual Service Service { get; set; }
        [NotMapped]
        public List<Role> Roles { get; set; }
        public int rol_id { get; set; }
        public int service_id { get; set; }
    }
}
