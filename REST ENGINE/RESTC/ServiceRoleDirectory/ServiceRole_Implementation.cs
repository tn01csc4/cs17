﻿using RESTC.RoleDirectory;
using RESTC.ServiceDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.ServiceRoleDirectory
{
    public class ServiceRole_Implementation
    {
        private Hrmodel db = new Hrmodel();

        public bool delete(int service_id)
        {
            try
            {
                List<ServiceRole> serviceRoles = db.serviceRoles.ToList();
                var query = from ServiceRole in serviceRoles where ServiceRole.service_id == service_id select ServiceRole;

                foreach (var r in query)
                {
                    db.serviceRoles.Remove(r);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool insert(ServiceRole t)
        {
            try
            {
                t.service_id = t.Service.id;
                foreach (Role r in t.Roles)
                {
                    t.rol_id = r.id;
                    db.serviceRoles.Attach(t);
                    db.Entry(t).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                }


                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public ServiceRole select(int service_id)
        {
            try
            {
                ServiceRepository repo1 = new ServiceRepository();
                Rolerepository rolerepository = new Rolerepository();
                ServiceRole serviceRole = new ServiceRole();
                List<ServiceRole> serviceRoles = select();
                var query = from ServiceRole in serviceRoles where ServiceRole.id == service_id select ServiceRole;
                serviceRole.Service = repo1.Get_Service_details(service_id);
                foreach (var item in query)
                {
                    serviceRole.Roles.Add(rolerepository.Get_Role(item.rol_id));
                }
                return serviceRole;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<ServiceRole> select()
        {
            try
            {
                List<ServiceRole> serviceRoles = db.serviceRoles.ToList();
                var query = from ServiceRole in serviceRoles group ServiceRole by ServiceRole.service_id into g select new { service_id = g.Key, rol_id = g.ToList() };
                List<ServiceRole> result = new List<ServiceRole>();
                foreach (var item in query)
                {
                    ServiceRole sr = new ServiceRole();
                    sr.Roles = new List<Role>();
                    sr.Service = new ServiceRepository().Get_Service_details(item.service_id);
                    foreach (ServiceRole role in item.rol_id)
                    {
                        sr.Roles.Add(new Rolerepository().Get_Role(role.rol_id));
                    }
                    result.Add(sr);
                }
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool update(ServiceRole t)
        {
            try
            {
                if (delete(t.Service.id))
                {
                    if (insert(t))
                    {
                        return true;
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
