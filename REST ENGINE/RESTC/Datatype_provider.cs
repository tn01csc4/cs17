﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC
{
    public class Datatype_provider
    {
        public static List<Datatypes> getAllDatatypes()
        {
            List<Datatypes> datatypes = new List<Datatypes>()
            {
                new Datatypes(){ Datatype="CHAR",islengthrequired=true},
                new Datatypes(){ Datatype="VARCHAR",islengthrequired=true},
                new Datatypes(){ Datatype="TEXT",islengthrequired=false},
                new Datatypes(){ Datatype="NVARCHAR",islengthrequired=true},
                new Datatypes(){ Datatype="NCHAR",islengthrequired=true},
                new Datatypes(){ Datatype="NTEXT",islengthrequired=false},
                new Datatypes(){ Datatype="BINARY",islengthrequired=true},
                new Datatypes(){ Datatype="VARBINARY",islengthrequired=true},
                new Datatypes(){ Datatype="IMAGE",islengthrequired=true},
                new Datatypes(){ Datatype="BIT",islengthrequired=false},
                new Datatypes(){ Datatype="TINYINT",islengthrequired=false},
                new Datatypes(){ Datatype="SMALLINT",islengthrequired=false},
                new Datatypes(){ Datatype="INT",islengthrequired=false},
                new Datatypes(){ Datatype="BIGINT",islengthrequired=false},
                new Datatypes(){ Datatype="REAL",islengthrequired=false},
                new Datatypes(){ Datatype="DECIMAL",islengthrequired=true},
                new Datatypes(){ Datatype="NUMERIC",islengthrequired=true},
                new Datatypes(){ Datatype="FLOAT",islengthrequired=true},
                new Datatypes(){ Datatype="DATETIME",islengthrequired=false},
                new Datatypes(){ Datatype="DATETIME2",islengthrequired=false},
                new Datatypes(){ Datatype="DATE",islengthrequired=false},
                new Datatypes(){ Datatype="TIME",islengthrequired=false},
                new Datatypes(){ Datatype="TIMESTAMP",islengthrequired=false},
            };
            return datatypes;
        }
        public static Boolean is_length_required(String propertytype)
        {
            List<Datatypes> datatypes = getAllDatatypes();
            var res = from Datatype in datatypes where Datatype.Datatype == propertytype select Datatype;
            return res.FirstOrDefault().islengthrequired;
        }
    }
}
