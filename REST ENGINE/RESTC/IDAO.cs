﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC
{
    public interface IDAO<T>
    {
        Boolean insert(T t);
        Boolean update(T t);
        Boolean delete(T t);
        T select(int id);
        List<T> select();
    }
}
