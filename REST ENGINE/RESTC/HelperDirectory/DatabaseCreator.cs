﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.HelperDirectory
{
    public class DatabaseCreator
    {

        public static String CreateDatabase(String Dbname)
        {
            String message = "";
            String str;
            SqlConnection myConn = new SqlConnection
   (@"Data Source=restc.cmqlmkckhxgt.us-east-2.rds.amazonaws.com;User ID=sa;Password=testtest;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            str = "CREATE DATABASE " + Dbname;
            SqlCommand myCommand = new SqlCommand(str, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                message += "Successfully Created";
                return message;
            }
            catch (System.Exception ex)
            {
                message += ex.Message;
                return message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
        public static String DropDatabase(String Dbname)
        {
            String message = "";
            SqlConnection myConn = new SqlConnection
           (@"Data Source=restc.cmqlmkckhxgt.us-east-2.rds.amazonaws.com;User ID=sa;Password=testtest;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlCommand myCommand = new SqlCommand("Drop Database " + Dbname, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                message += "Successfully Droped";
                return message;
            }
            catch (System.Exception ex)
            {
                message += ex.Message;
                return message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
        public static String RenameDatabase(String Dbname, String NewName)
        {
            String message = "";
            SqlConnection myConn = new SqlConnection
           (@"Data Source=restc.cmqlmkckhxgt.us-east-2.rds.amazonaws.com;User ID=sa;Password=testtest;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            SqlCommand myCommand = new SqlCommand("ALTER DATABASE " + Dbname + " MODIFY NAME = " + NewName, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                message += "Successfully Rename";
                return message;
            }
            catch (System.Exception ex)
            {
                message += ex.Message;
                return message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
    }
}
