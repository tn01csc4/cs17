﻿using RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory;
using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.HelperDirectory
{
    public class Dynamic_DML_Commands
    {
        Hrmodel db = new Hrmodel();
        Db_tableRepository repo = new Db_tableRepository();
        DB_Table_fieldRepository fieldrepo = new DB_Table_fieldRepository();
        public List<Dictionary<String, Object>> Getvalues(int table_id)
        {
            List<Dictionary<String, Object>> Result = new List<Dictionary<string, object>>();

            Db_tables table = repo.Get_Table_Record(table_id);
            List<Table_fields_constraints> fields = fieldrepo.Get_All_Table_Field_details(table.id);

            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            String selectquery = "Select * from " + table.Name;
            SqlCommand command = new SqlCommand(selectquery, myConn);
            try
            {
                myConn.Open();
                SqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Dictionary<String, Object> value = new Dictionary<string, object>();
                    foreach (Table_fields_constraints item in fields)
                    {
                        value.Add(item.Field.Name, dr[item.Field.Name]);
                    }
                    Result.Add(value);
                }

                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();

                }
            }
        }
        public Dictionary<String, Object> Getvalues(int table_id, int primarykeyvalue)
        {
            Dictionary<String, Object> Result = new Dictionary<string, object>();
            Db_tables table = repo.Get_Table_Record(table_id);
            List<Table_fields_constraints> fields = fieldrepo.Get_All_Table_Field_details(table.id);
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            List<Constraints> constraints = new Hrmodel().constraints.ToList();
            var Field = from Constraint in constraints where Constraint.Name == "Primary key" && Constraint.Field.PropertyType == "INT" && Constraint.Field.Table_id == table.id select Constraint.Field;
            String selectquery = "Select * from " + table.Name + " where " + Field.FirstOrDefault().Name + "='" + primarykeyvalue + "'";
            SqlCommand command = new SqlCommand(selectquery, myConn);
            try
            {
                myConn.Open();
                SqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    foreach (Table_fields_constraints item in fields)
                    {
                        Result.Add(item.Field.Name, dr[item.Field.Name]);
                    }
                }
                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();

                }
            }
        }
        public String deleteRecord(int table_id, Dictionary<String, Object> Data)
        {
            if (Data.Keys.Count() > 0)
            {
                Db_tables table = repo.Get_Table_Record(table_id);
                List<Table_fields_constraints> fields = fieldrepo.Get_All_Table_Field_details(table.id);

                SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
                String deletequery = "delete from " + table.Name + " where";
                foreach (String key in Data.Keys)
                {
                    var a = from F in fields where F.Field.Name.ToLower() == key.ToLower() select F;
                    if (a.Count() > 0)
                    {
                        if (Data[key] != null)
                        {
                            deletequery += " " + key + "='" + Data[key] + "' And";
                        }
                    }
                }
                deletequery = deletequery.Substring(0, deletequery.Length - 3);
                try
                {
                    SqlCommand command = new SqlCommand(deletequery, myConn);
                    myConn.Open();
                    command.ExecuteNonQuery();
                    return "deleted";

                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                finally
                {
                    if (myConn.State == ConnectionState.Open)
                    {
                        myConn.Close();
                    }
                }
            }
            else
            {
                return "Data Not Found";
            }
        }
        public String insertRecord(int table_id, Dictionary<String, Object> Data)
        {
            if (Data.Keys.Count > 0)
            {
                Db_tables table = repo.Get_Table_Record(table_id);
                List<Table_fields_constraints> fields = fieldrepo.Get_All_Table_Field_details(table.id);
                SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
                String insertquery = "insert into " + table.Name;
                String Columns = "(";
                String Values = "Values(";
                foreach (String Key in Data.Keys)
                {
                    var a = from F in fields where F.Field.Name.ToLower() == Key.ToLower() select F;
                    if (a != null)
                    {
                        List<Constraints> cns1 = db.constraints.ToList();
                        var query3 = from Constraints in cns1 where Constraints.Field.Table_id == table.id && Constraints.Name == "Primary key" select Constraints;
                        if (query3.Count() == 1 && query3.FirstOrDefault().Field.Name.ToLower() == Key.ToLower())
                        {

                        }
                        else
                        {
                            Columns += Key + ",";
                            Values += "'" + Data[Key] + "',";
                        }
                    }
                }
                Columns = Columns.Substring(0, Columns.Length - 1);
                Values = Values.Substring(0, Values.Length - 1);
                insertquery += " " + Columns + ") " + Values + ")";
                try
                {
                    SqlCommand command = new SqlCommand(insertquery, myConn);
                    myConn.Open();
                    command.ExecuteNonQuery();
                    return "Inserted";
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                finally
                {
                    if (myConn.State == ConnectionState.Open)
                    {
                        myConn.Close();
                    }
                }
            }
            else
            {
                return "No Object Found";
            }
        }
        public String deleteRecord(int table_id, int primarykeyvalue)
        {
            Db_tables table = repo.Get_Table_Record(table_id);
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            List<Constraints> constraints = new Hrmodel().constraints.ToList();
            var Field = from Constraint in constraints where Constraint.Name == "Primary key" && Constraint.Field.PropertyType == "INT" && Constraint.Field.Table_id == table.id select Constraint.Field;
            String Deletequery = "Delete from " + table.Name + " where " + Field.FirstOrDefault().Name + "='" + primarykeyvalue + "'";
            try
            {
                SqlCommand command = new SqlCommand(Deletequery, myConn);
                myConn.Open();
                command.ExecuteNonQuery();
                return "deleted";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
        public String UpdateRecord(int table_id, int primarykeyvalue, Dictionary<String, Object> Data)
        {
            if (Data.Keys.Count > 0)
            {
                Db_tables table = repo.Get_Table_Record(table_id);
                List<Table_fields_constraints> fields = fieldrepo.Get_All_Table_Field_details(table.id);
                SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
                List<Constraints> constraints = new Hrmodel().constraints.ToList();
                var Field = from Constraint in constraints where Constraint.Name == "Primary key" && Constraint.Field.PropertyType == "INT" && Constraint.Field.Table_id == table.id select Constraint.Field;
                String updatequery = "Update " + table.Name + " set ";
                foreach (String key in Data.Keys)
                {
                    Boolean validcolumn = false;
                    foreach (Table_fields_constraints item in fields)
                    {
                        if (item.Field.Name.ToLower() == key.ToLower())
                        {
                            validcolumn = true;
                            break;
                        }
                    }
                    if (validcolumn)
                    {
                        updatequery += key + " ='" + Data[key] + "',";
                    }
                }
                updatequery = updatequery.Substring(0, updatequery.Length - 1);
                updatequery += " where " + Field.FirstOrDefault().Name + "='" + primarykeyvalue + "'";
                try
                {
                    SqlCommand command = new SqlCommand(updatequery, myConn);
                    myConn.Open();
                    command.ExecuteNonQuery();
                    return "updated";

                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                finally
                {
                    if (myConn.State == ConnectionState.Open)
                    {
                        myConn.Close();
                    }
                }
            }
            return "Failed";
        }
        public List<Dictionary<String, Object>> Getvalues(int table_id, String AttributeName, String val)
        {
            List<Dictionary<String, Object>> Result = new List<Dictionary<string, object>>();
            Db_tables table = repo.Get_Table_Record(table_id);
            List<Table_fields_constraints> fields = fieldrepo.Get_All_Table_Field_details(table.id);
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            String selectquery = "select * from " + table.Name + " where " + AttributeName + "='" + val + "'";
            try
            {
                SqlCommand command = new SqlCommand(selectquery, myConn);
                myConn.Open();
                SqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Dictionary<String, Object> value = new Dictionary<string, object>();
                    foreach (Table_fields_constraints item in fields)
                    {
                        value.Add(item.Field.Name, dr[item.Field.Name]);
                    }
                    Result.Add(value);
                }

                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();

                }
            }
        }
        public List<Dictionary<String, Object>> Getvalues(int table_id, String Condition)
        {
            List<Dictionary<String, Object>> Result = new List<Dictionary<string, object>>();
            Db_tables table = repo.Get_Table_Record(table_id);
            List<Table_fields_constraints> fields = fieldrepo.Get_All_Table_Field_details(table.id);
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            String selectquery = "select * from " + table.Name + " where " + Condition;
            try
            {
                SqlCommand command = new SqlCommand(selectquery, myConn);
                myConn.Open();
                SqlDataReader dr = command.ExecuteReader();

                while (dr.Read())
                {
                    Dictionary<String, Object> value = new Dictionary<string, object>();
                    foreach (Table_fields_constraints item in fields)
                    {
                        value.Add(item.Field.Name, dr[item.Field.Name]);
                    }
                    Result.Add(value);
                }

                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();

                }
            }
        }
        public String deleteRecord(int table_id, String AttributeName, String val)
        {
            Db_tables table = repo.Get_Table_Record(table_id);
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            String Deletequery = "Delete from " + table.Name + " where " + AttributeName + "='" + val + "'";
            try
            {
                SqlCommand command = new SqlCommand(Deletequery, myConn);
                myConn.Open();
                command.ExecuteNonQuery();
                return "deleted";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
        public String UpdateRecord(int table_id, String AttributeName, String val, Dictionary<String, Object> Data)
        {
            if (Data.Keys.Count > 0)
            {
                Db_tables table = repo.Get_Table_Record(table_id);
                List<Table_fields_constraints> fields = fieldrepo.Get_All_Table_Field_details(table.id);
                SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
                String updatequery = "Update " + table.Name + " set ";
                foreach (String key in Data.Keys)
                {
                    Boolean validcolumn = false;
                    foreach (Table_fields_constraints item in fields)
                    {
                        if (item.Field.Name.ToLower() == key.ToLower())
                        {
                            validcolumn = true;
                            break;
                        }
                    }
                    if (validcolumn)
                    {
                        updatequery += key + " ='" + Data[key] + "',";
                    }
                }
                updatequery = updatequery.Substring(0, updatequery.Length - 1);
                updatequery += " where " + AttributeName + "='" + val + "'";
                try
                {
                    SqlCommand command = new SqlCommand(updatequery, myConn);
                    myConn.Open();
                    command.ExecuteNonQuery();
                    return "updated";

                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                finally
                {
                    if (myConn.State == ConnectionState.Open)
                    {
                        myConn.Close();
                    }
                }
            }
            return "Failed";
        }

    }
}
