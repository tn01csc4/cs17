﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.HelperDirectory
{
    public class ApikeyGenerator
    {
        public static String getkey(String rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                Random random = new Random();
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                //Console.WriteLine(bytes.ToString());
                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    // Console.WriteLine(bytes[i]+random.Next());
                    builder.Append(bytes[i] + random.Next(0, 2000).ToString("x"));
                    //Console.WriteLine(builder.ToString());
                }
                return builder.ToString().Substring(0, 64);
            }
        }
    }
}
