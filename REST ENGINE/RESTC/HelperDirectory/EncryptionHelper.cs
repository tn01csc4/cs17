﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.HelperDirectory
{
    public class EncryptionHelper
    {
        public static String Get_Encrypted_password(String Password, int key)
        {
            String pass = Password;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < pass.Length; j++)
                {
                    pass = pass.Replace(pass.ElementAt(j), (char)((pass.ElementAt(j) + key) % 255));
                }
            }
            return pass;
        }
    }
}


