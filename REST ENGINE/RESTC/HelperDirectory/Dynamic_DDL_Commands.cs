﻿using RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory;
using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.HelperDirectory
{
    public class Dynamic_DDL_Commands
    {
        Hrmodel db = new Hrmodel();
        Db_tableRepository TableRepository = new Db_tableRepository();
        public String Create_table(List<Table_fields_constraints> db_Table_Fields)
        {
            Db_tables table = TableRepository.Get_Table_Record(db_Table_Fields[0].Field.Table_id);
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            String Createtablecommand = @"create table " + table.Name + "(";
            foreach (Table_fields_constraints tfc in db_Table_Fields)
            {
                Createtablecommand += tfc.Field.Name + " " + tfc.Field.PropertyType;
                Createtablecommand += Datatype_provider.is_length_required(tfc.Field.PropertyType) && tfc.Field.Length != 0 ? "(" + tfc.Field.Length + ")" : " ";
                List<Constraints> cns1 = db.constraints.ToList();
                var query3 = from Constraints in cns1 where Constraints.Field.Table_id == table.id && Constraints.Name == "Primary key" select Constraints;
                if (query3.Count() == 1 && tfc.Field.PropertyType == "INT" && query3.FirstOrDefault().Field.id == tfc.Field.id)
                {
                    Createtablecommand += "IDENTITY(1,1)";
                }
                foreach (Constraints c in tfc.constraints)
                {
                    if (c.Name == "Primary key" || c.Name == "Foreign key")
                    {
                        break;
                    }
                    else
                    {
                        Createtablecommand += " ";
                        Createtablecommand += c.get_Constraint_query_();
                    }
                }
                Createtablecommand += ",";
            }
            List<Constraints> cns = db.constraints.ToList();
            var query = from Constraints in cns where Constraints.Field.Table_id == table.id && Constraints.Name == "Primary key" select Constraints;
            if (query.Count() > 1)
            {
                String pk_constraint = "Constraint PK_" + table.Name + " Primary key (";
                foreach (Constraints constraint in query)
                {
                    pk_constraint += constraint.Field.Name + ",";
                }
                Createtablecommand += pk_constraint.Substring(0, pk_constraint.Length - 1) + "),";

            }
            else
            {
                if (query.Count() == 1)
                {
                    Createtablecommand += query.FirstOrDefault().get_Constraint_query_() + ",";
                }
            }
            var query1 = from Constraints in cns where Constraints.Field.Table_id == table.id && Constraints.Name == "Foreign key" select Constraints;
            if (query1.Count() >= 1)
            {
                foreach (Constraints constraint in query1)
                {
                    Createtablecommand += constraint.get_Constraint_query_() + ",";
                }
            }
            Createtablecommand = Createtablecommand.Substring(0, Createtablecommand.Length);
            Createtablecommand += ")";
            Console.WriteLine(Createtablecommand);
            SqlCommand myCommand = new SqlCommand(Createtablecommand, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                return "Successfully created";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
        public String DropTable(Db_tables table)
        {
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            String Droptablequery = "Drop table " + table.Name;
            SqlCommand myCommand = new SqlCommand(Droptablequery, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                return "Successfully Droped";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
        public String RenameTable(String Old_Name, Db_tables table)
        {
            ServiceDirectory.ServiceRepository repo = new ServiceDirectory.ServiceRepository();
            ServiceDirectory.Service Service = repo.Get_Service_details(table.service_id);
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            String Renametablequery = "EXEC sp_rename '" + Old_Name + "','" + table.Name + "'";
            SqlCommand myCommand = new SqlCommand(Renametablequery, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                return "Successfully Renamed";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
        public String DropColumn(Db_Table_Field field)
        {
            Db_tableRepository repository = new Db_tableRepository();
            Db_tables table = repository.Get_Table_Record(field.Table_id);
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            String DropColumnquery = "Alter table " + table.Name + " drop column " + field.Name;
            SqlCommand myCommand = new SqlCommand(DropColumnquery, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                return "Column Droped";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }

        }
        public String AddColumn(Table_fields_constraints table_Fields_)
        {
            Db_tables table = TableRepository.Get_Table_Record(table_Fields_.Field.Table_id);
            SqlConnection myConn = new SqlConnection(Connection.GetConnectionString(table));
            String Altertablecommand = "Alter table " + table.Name + " Add " + table_Fields_.Field.Name + " " + table_Fields_.Field.PropertyType;
            Altertablecommand += Datatype_provider.is_length_required(table_Fields_.Field.PropertyType) ? "(" + table_Fields_.Field.Length + ")" : "";
            foreach (Constraints c in table_Fields_.constraints)
            {
                Altertablecommand += " " + c.get_Constraint_query_();
            }
            SqlCommand myCommand = new SqlCommand(Altertablecommand, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                return "Column Added";
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
    }
}
