﻿using RESTC.SystemCorsDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.HelperDirectory
{
    public class Corsprovider
    {

        public static String getorigin()
        {
            Corsrepository corsrepository = new Corsrepository();
            String origin = "";
            List<Corsdetails> corsdetails = corsrepository.Get_All_Cors_config_details();
            corsdetails.ForEach(x => origin += x.Origin + ",");
            return "*";
        }
        public static String getheader()
        {
            Corsrepository corsrepository = new Corsrepository();
            String header = "";
            List<Corsdetails> corsdetails = corsrepository.Get_All_Cors_config_details();
            corsdetails.ForEach(x => header += x.Header + ",");
            return "*";
        }
        public static String getmethods()
        {
            Corsrepository corsrepository = new Corsrepository();
            String Methods = "";
            List<Corsdetails> corsdetails = corsrepository.Get_All_Cors_config_details();
            corsdetails.ForEach(x => Methods += x.Header + ",");
            return "*";
        }
        public static String getexposedheader()
        {
            Corsrepository corsrepository = new Corsrepository();
            String Exposedheader = "";
            List<Corsdetails> corsdetails = corsrepository.Get_All_Cors_config_details();
            corsdetails.ForEach(x => Exposedheader += x.ExposedHeader + ",");
            return "*";
        }
    }
}
