﻿using RESTC.HelperDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.UserDirectory
{
    public class UserRepository
    {
        private User_IDAO dao = new User_Implementation();
        public Boolean Add_User(User user)
        {
            try
            {
                if (user != null)
                {
                    Random random = new Random();
                    user.createddate = DateTime.Now;
                    user.lastmodifieddate = DateTime.Now;
                    user.is_active = true;
                    user.key = random.Next(0, 1000);
                    user.password = EncryptionHelper.Get_Encrypted_password(user.password, user.key);
                    return dao.insert(user);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public User Get_User(int id)
        {
            return dao.select(id);
        }

        public Boolean Modify_User(User user)
        {
            try
            {
                if (user != null)
                {
                    if (user.Newpassword != null)
                    {
                        Random random = new Random();
                        user.key = random.Next(0, 1000);
                        user.password = EncryptionHelper.Get_Encrypted_password(user.Newpassword, user.key);
                    }
                    return dao.update(user);
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Remove_User(User user)
        {
            try
            {
                if (user != null)
                {
                    return dao.delete(user);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public User Get_User(String Username, String Password)
        {

            try
            {

                return dao.select(Username, Password);
            }
            catch (Exception e)
            {
                return null;
            }

        }
        public User Get_User(String Email)
        {

            try
            {
                return dao.select(Email);
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public User GetUserByUsername(String Username)
        {
            User user = null;
            List<User> users = dao.select();
            var query = from User in users where User.username == Username select User;
            foreach (var item in query)
            {
                user = item;
            }
            return user;
        }
        public List<User> GetAllUsers()
        {
            List<User> users = new List<User>();
            List<User> users1 = dao.select();
            var query = from User in users1 where User.is_sysadmin != true select User;
            foreach (var item in query)
            {
                users.Add(item);
            }
            return users;
        }


    }
}
