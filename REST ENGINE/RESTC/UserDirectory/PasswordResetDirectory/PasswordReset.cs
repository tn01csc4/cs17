﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace RESTC.UserDirectory.PasswordResetDirectory
{
    public class PasswordReset
    {
        public int id { get; set; }
        public virtual User user { get; set; }
        public String Token { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime created_at { get; set; }
    }
}
