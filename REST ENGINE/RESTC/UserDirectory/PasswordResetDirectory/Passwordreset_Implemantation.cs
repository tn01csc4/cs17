﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.UserDirectory.PasswordResetDirectory
{
    public class Passwordreset_Implemantation
    {
        private Hrmodel db = new Hrmodel();
        public Boolean delete(PasswordReset passwordReset)
        {
            try
            {
                if (passwordReset != null)
                {
                    db.passwordResets.Remove(passwordReset);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean update(PasswordReset passwordReset)
        {
            try
            {
                if (passwordReset != null)
                {
                    db.passwordResets.Attach(passwordReset);
                    db.Entry(passwordReset).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean insert(PasswordReset passwordReset)
        {
            try
            {
                if (passwordReset != null)
                {
                    db.passwordResets.Add(passwordReset);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public PasswordReset select(String Token, String email)
        {
            List<PasswordReset> passwordResets = get_All_Password_Request();
            PasswordReset passwordReset = null;
            if (passwordResets != null)
            {
                var query = from PasswordReset in passwordResets where PasswordReset.Token == Token && PasswordReset.user.email == email select PasswordReset;
                if (query.Count() == 1)
                {
                    foreach (var x in query)
                    {
                        passwordReset = x;
                    }
                }
            }
            return passwordReset;
        }
        public List<PasswordReset> get_All_Password_Request()
        {
            try
            {
                return db.passwordResets.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}
