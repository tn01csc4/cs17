﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.UserDirectory.PasswordResetDirectory
{
    public class PasswordResetRepository
    {
        private Passwordreset_Implemantation dao = new Passwordreset_Implemantation();
        public Boolean Add_PasswordReset_Request(PasswordReset passwordReset)
        {
            try
            {
                if (passwordReset != null)
                {
                    return dao.insert(passwordReset);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Modify_PasswordReset_Request(PasswordReset passwordReset)
        {
            try
            {
                if (passwordReset != null)
                {
                    return dao.update(passwordReset);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Remove_PasswordReset_Request(PasswordReset passwordReset)
        {
            try
            {
                if (passwordReset != null)
                {
                    return dao.delete(passwordReset);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public PasswordReset Get_PasswordReset_Request(String Token, String Email)
        {
            try
            {
                return dao.select(Token, Email);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public Boolean IsPasswordRequestAlreadyAvailable(String email)
        {
            try
            {
                var query = from item in dao.get_All_Password_Request() where item.user.email == email select item;
                if (query.Count() == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public PasswordReset Get_PasswordReset_Request(String email)
        {
            try
            {
                var query = from item in dao.get_All_Password_Request() where item.user.email == email select item;
                if (query.Count() == 0)
                {
                    return null;
                }
                else
                {
                    PasswordReset passwordReset = null;
                    foreach (var item in query)
                    {
                        passwordReset = item;
                    }
                    return passwordReset;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
