﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.UserDirectory
{
    public interface User_IDAO
    {
        Boolean insert(User user);
        Boolean delete(User user);
        Boolean update(User user);
        User select(String Email);
        User select(int id);
        User select(String Username, String Password);
        List<User> select();
    }
}