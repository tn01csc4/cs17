﻿using RESTC.HelperDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.UserDirectory
{
    public class User_Implementation : User_IDAO
    {
        Hrmodel db = new Hrmodel();
        public Boolean delete(User user)
        {
            try
            {
                if (user != null)
                {
                    db.users.Remove(user);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean insert(User user)
        {
            try
            {
                db.users.Add(user);
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public User select(string Username, string Password)
        {
            User u = null;
            try
            {
                List<User> users = select();

                var Query = from User in users where User.username == Username select User;
                var Query2 = from item in Query
                             where
           item.password == EncryptionHelper.Get_Encrypted_password(Password, item.key)
                             select item;
                if (Query2.Count() > 0)
                {
                    foreach (var x in Query2)
                    {
                        u = x;

                    }
                }
                return u;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public List<User> select()
        {
            return db.users.ToList();
        }

        public User select(string Email)
        {
            User u = null;
            try
            {
                List<User> users = select();
                var Query = from User in users where User.email == Email select User;
                if (Query.Count() == 1)
                {
                    foreach (var x in Query)
                    {
                        u = x;

                    }
                }
                return u;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean update(User user)
        {
            try
            {

                db.users.Attach(user);
                db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public User select(int id)
        {
            return db.users.Find(id);
        }
    }

}
