﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RESTC.UserDirectory
{
    public class User
    {
        public int id { get; set; }
        [Required(ErrorMessage = "You need to provide Display Name of User"), Display(Name = "Display Name")]
        public String name { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "You need to provide Firstname of User")]//Applying Server side validation on property Firstname
        public String firstname { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "You need to provide lastname of User")]//Applying Server side validation on property Lastname
        public String Lastname { get; set; }
        [Display(Name = "Email Address")]
        [EmailAddress(ErrorMessage = "Provide a valid Email id")]
        public String email { get; set; }
        [Display(Name = "UserName")]
        [RegularExpression("^[A-Z][a-z0-9]{5,12}$", ErrorMessage = "Provide a valid Username"), Required]
        public String username { get; set; }
        [Required(ErrorMessage = "Password field cannot be empty")]
        [DataType(DataType.Password)]
        public String password { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime lastlogindate { get; set; }
        public Boolean is_sysadmin { get; set; }
        public Boolean is_active { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime createddate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime lastmodifieddate { get; set; }
        [Display(Name = "Security Question")]
        public String security_ques { get; set; }
        [Display(Name = "Security Answer")]
        public String seurity_answer { get; set; }
        public int key { get; set; }
        [NotMapped]
        public String Newpassword { get; set; }
    }
}
