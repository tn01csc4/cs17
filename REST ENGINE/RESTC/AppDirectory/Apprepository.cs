﻿using RESTC.UserDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RESTC.HelperDirectory;
namespace RESTC.AppDirectory
{
    public class Apprepository
    {
        private IDAO<App> dao = new App_Implementation();
        public Boolean Set_App_details(App app)
        {
            try
            {
                if (app != null)
                {
                    app.created_date = DateTime.Now;
                    app.Lastmodifydate = DateTime.Now;
                    app.Api_key = ApikeyGenerator.getkey(app.Name);
                    return dao.insert(app);
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Remove_app_details(App app)
        {
            try
            {
                if (app != null)
                {
                    return dao.delete(app);
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public Boolean Modify_app_details(App app)
        {
            try
            {
                if (app != null)
                {
                    app.Lastmodifydate = DateTime.Now;
                    return dao.update(app);
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
        public App Get_App_details(int id)
        {
            try
            {
                return dao.select(id);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<App> Get_All_Apps_details()
        {
            try
            {
                return dao.select();
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<App> Get_All_Apps_details(User user)
        {
            if (user.is_sysadmin)
            {
                return Get_All_Apps_details();
            }
            else
            {
                List<App> apps = Get_All_Apps_details();
                var query = from app in apps where app.Createdby == user select app;
                List<App> list = new List<App>();
                foreach (var item in query)
                {
                    list.Add(item);
                }
                return list;
            }
        }
    }
}
