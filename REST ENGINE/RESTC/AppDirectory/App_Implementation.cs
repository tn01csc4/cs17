﻿using RESTC.ServiceDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.AppDirectory
{
    public class App_Implementation : IDAO<App>
    {
        private Hrmodel db = new Hrmodel();
        public Boolean delete(App app)
        {
            try
            {
                if (app != null)
                {
                    List<Service> services = db.services.ToList();
                    var query = from Service in services where Service.app_id == app.id select Service;
                    if (query.Count() == 0)
                    {
                        db.apps.Remove(app);
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean insert(App app)
        {
            try
            {
                if (app != null)
                {
                    db.apps.Attach(app);
                    db.Entry(app).State = System.Data.Entity.EntityState.Added;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public App select(int id)
        {
            try
            {
                App app = (db.apps.Find(id));
                return app;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<App> select()
        {
            try
            {
                return db.apps.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean update(App app)
        {
            try
            {
                db.apps.Attach(app);
                db.Entry(app).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
    }
}
