﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RESTC.UserDirectory;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace RESTC.AppDirectory
{
    public class App
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Provide app Name")]
        public String Name { get; set; }
        public String Description { get; set; }
        public String Api_key { get; set; }
        public Boolean isactive { get; set; }
        [Required]
        public virtual User Createdby { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime created_date { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Lastmodifydate { get; set; }
    }
}
