﻿using RESTC.UserDirectory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.ServiceDirectory
{
    public class Service
    {
        public int id { get; set; }
        [Required]
        public String Name { get; set; }
        public String Label { get; set; }
        public Boolean isactive { get; set; }

        public String Description { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime createddate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime lastmodifieddate { get; set; }
        public int app_id { get; set; }
        public virtual User User { get; set; }
    }
}
