﻿using RESTC.ServiceRoleDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.ServiceDirectory
{
    public class ServiceRepository
    {

        private IDAO<Service> dao = new Service_Implementation();
        private ServiceRole_Implementation dao1 = new ServiceRole_Implementation();
        public Boolean Set_Service_details(ServiceRole service)
        {
            try
            {
                if (service.Service != null)
                {
                    service.Service.createddate = DateTime.Now;
                    service.Service.lastmodifieddate = DateTime.Now;
                    if (dao.insert(service.Service))
                    {
                        if (dao1.insert(service))
                        {
                            return true;
                        }
                        else
                        {
                            dao.delete(service.Service);
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public Boolean Modify_Service_details(ServiceRole service)
        {
            try
            {
                if (service.Service != null)
                {
                    if (dao.update(service.Service))
                    {
                        return dao1.update(service);
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public Boolean Remove_Service_details(int service_id)
        {
            try
            {
                Service service = Get_Service_details(service_id);
                if (service != null)
                {
                    if (dao.delete(service))
                    {
                        if (dao1.delete(service_id))
                        {
                            return true;
                        }

                    }
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public Service Get_Service_details(int id)
        {
            try
            {
                return dao.select(id);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public List<Service> Get_All_Services_details()
        {
            try
            {
                return dao.select();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<ServiceRole> Get_All_ServiceRole()
        {
            try
            {
                return dao1.select();
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public ServiceRole Get_ServiceRole(int service_id)
        {
            try
            {
                return dao1.select(service_id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
