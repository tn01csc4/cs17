﻿using RESTC.HelperDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTC.ServiceDirectory
{
    public class Service_Implementation : IDAO<Service>
    {
        private Hrmodel db = new Hrmodel();
        public Boolean delete(Service service)
        {
            try
            {
                if (service != null)
                {
                    db.services.Remove(service);
                    if (DatabaseCreator.DropDatabase(service.Name) == "Successfully Droped")
                    {
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Boolean insert(Service service)
        {
            try
            {
                if (service != null)
                {
                    db.services.Attach(service);
                    db.Entry(service).State = System.Data.Entity.EntityState.Added;
                    if (DatabaseCreator.CreateDatabase(service.Name) == "Successfully Created")
                    {
                        db.SaveChanges();

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }

        public Service select(int id)
        {
            try
            {
                return db.services.Find(id);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Service> select()
        {
            try
            {

                return db.services.ToList(); ;


            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Boolean update(Service service)
        {
            try
            {
                if (service != null)
                {
                    string name = select(service.id).Name;

                    var serv = select(service.id);
                    serv.Name = service.Name;
                    serv.Label = service.Label;
                    serv.Description = service.Description;
                    serv.isactive = service.isactive;
                    serv.lastmodifieddate = DateTime.Now;
                    serv.app_id = service.app_id;
                    if (name != service.Name)
                    {
                        DatabaseCreator.RenameDatabase(name, service.Name);
                    }
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.InnerException);
                return false;
            }
        }
    }
}
