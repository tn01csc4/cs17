namespace RESTC
{
    using RESTC.UserDirectory;
    using RESTC.SystemCorsDirectory;
    using RESTC.UserDirectory.PasswordResetDirectory;
    using System;
    using System.Data.Entity;
    using System.Linq;
    
   
    using RESTC.Email_templateDirectory;

    using RESTC.AppDirectory;
    using RESTC.RoleDirectory;
    using RESTC.ServiceDirectory;
    using RESTC.ServiceRoleDirectory;
    using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
    using RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory;

    public class Hrmodel : DbContext
    {
        
        public Hrmodel()
            : base("Hrmodel")
        {
        }
        public DbSet<App> apps { get; set; }
        public DbSet<User> users { get; set; }
        public DbSet<PasswordReset> passwordResets { get; set; }
        public DbSet<Corsdetails> cors { get; set; }
        public DbSet<Email_template> email_Templates { get; set; }
        public DbSet<Role> roles { get; set; }
        public DbSet<Service> services { get; set; }
        public DbSet<Db_tables> db_Tables { get; set; }
        public DbSet<Db_Table_Field> db_Table_Fields { get; set; }
        public DbSet<Constraints> constraints { get; set; }
        public DbSet<ServiceRole> serviceRoles { get; set; }
        public DbSet<UserAuthentication> user_auth { get; set; }
    }
}