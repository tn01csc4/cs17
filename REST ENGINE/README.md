## Information
REST ENGINE is the solution project
In REST ENGINE two projects are created
1. RESCTC which is the class library
2. REST_CONTROLLER which is the web Application

---
## RESTC
It's a class library.
We will create all the classes of model and the all the business logic will return in it. 
After that We will add reference of this project to Our Web Application REST_CONTROLLER.

---
## REST_CONTROLLER
All the controllers and views will get created in this application.

---

**Note**
1. When first time you clone the project create RESTC database in Sql server
2. Everytime you pull the project update your database with nuget package console in visual studio as the structure of the database might get change 


