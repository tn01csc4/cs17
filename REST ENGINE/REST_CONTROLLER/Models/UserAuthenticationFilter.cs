﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using RESTC.UserDirectory;
using RESTC;

namespace REST_CONTROLLER.Models
{
    public class UserAuthenticationFilter : ActionFilterAttribute
    {

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                String authenticatekey = actionContext.Request.Headers.GetValues("authenticatekey").FirstOrDefault();

                Hrmodel db = new Hrmodel();
                UserRepository userRepository = new UserRepository();
                List<UserAuthentication> users = db.user_auth.ToList();
                User u = (from User in users where User.user.username == authenticatekey select User.user).FirstOrDefault();
                if (u != null)
                {
                    actionContext.Request.Headers.Add("user_id", Convert.ToString(u.id));
                    base.OnActionExecuting(actionContext);
                }
                else
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage();
                    actionContext.Response.StatusCode = System.Net.HttpStatusCode.Unauthorized;

                }
            }
            catch (Exception ex)
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage();
                actionContext.Response.StatusCode = System.Net.HttpStatusCode.Unauthorized;

            }
        }
    }
}