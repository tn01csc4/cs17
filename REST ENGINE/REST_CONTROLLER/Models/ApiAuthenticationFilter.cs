﻿using RESTC;
using RESTC.AppDirectory;
using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
using RESTC.RoleDirectory;
using RESTC.ServiceDirectory;
using RESTC.ServiceRoleDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;


namespace REST_CONTROLLER.Models
{
    public class ApiAuthenticationFilter : ActionFilterAttribute
    {
        Hrmodel hrmodel = new Hrmodel();
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                String Apikey = actionContext.Request.Headers.GetValues("api_key").FirstOrDefault();
                List<App> apps = hrmodel.apps.ToList();
                var App = from Apps in apps where Apps.Api_key == Apikey select Apps;
                if (App.Count() == 1)
                {
                    if (App.FirstOrDefault().isactive)
                    {
                        List<Service> services = hrmodel.services.ToList();
                        var Service = from Services in services where Services.app_id == App.FirstOrDefault().id select Services;
                        if (Service.Count() >= 1)
                        {
                            IDictionary<String, object> TableName = actionContext.RequestContext.RouteData.Values;
                            List<Db_tables> dB_Tables = hrmodel.db_Tables.ToList();
                            var NewService = from Serve in Service join Db_Table in dB_Tables on Serve.id equals Db_Table.service_id where Db_Table.Name.ToLower() == ((String)TableName["Table"]).ToLower() select new { Serve, Db_Table };
                            if (NewService.Count() == 1)
                            {
                                if (NewService.FirstOrDefault().Serve.isactive)
                                {
                                    List<ServiceRole> serviceRoles = hrmodel.serviceRoles.ToList();
                                    var item = from ServiceRole in serviceRoles where ServiceRole.service_id == NewService.FirstOrDefault().Serve.id select ServiceRole;
                                    if (item.Count() >= 1)
                                    {
                                        Boolean ismethodallowed = false;
                                        foreach (var x in item)
                                        {
                                            Role role = hrmodel.roles.Find(x.rol_id);
                                            if (role.isactive)
                                            {
                                                String method = actionContext.Request.Method.Method;
                                                if (method.ToLower() == "get" && role._GET)
                                                {
                                                    ismethodallowed = true;
                                                    break;
                                                }
                                                if (method.ToLower() == "put" && role._PUT)
                                                {
                                                    ismethodallowed = true;
                                                    break;
                                                }
                                                if (method.ToLower() == "post" && role._POST)
                                                {
                                                    ismethodallowed = true;
                                                    break;
                                                }
                                                if (method.ToLower() == "delete" && role._DELETE)
                                                {
                                                    ismethodallowed = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (ismethodallowed)
                                        {
                                            actionContext.Request.Headers.Add("table_id", Convert.ToString(NewService.FirstOrDefault().Db_Table.id));
                                            base.OnActionExecuting(actionContext);
                                        }
                                        else
                                        {
                                            actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.MethodNotAllowed);

                                        }
                                    }
                                    else
                                    {
                                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotAcceptable);

                                    }
                                }
                                else
                                {
                                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotAcceptable);

                                }
                            }
                            else
                            {
                                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Ambiguous);

                            }
                        }
                        else
                        {
                            actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotAcceptable);

                        }
                    }
                    else
                    {
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NoContent);

                    }
                }
                else
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);

                }
            }
            catch (Exception ex)
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
        }
    }
}