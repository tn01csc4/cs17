﻿using RESTC;
using RESTC.Email_templateDirectory;
using RESTC.UserDirectory;
using RESTC.UserDirectory.PasswordResetDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace REST_CONTROLLER.Controllers
{
    public class HomeController : Controller
    {
        private UserRepository userRepository;
        public ActionResult Index()
        {
            if (Session["User"] != null)
            {
                return RedirectToAction("index", "Homepage");
            }
            else
            {
                return View("Login");
            }
        }

        [AllowAnonymous]
        public ActionResult checkuser(User user)
        {
            try
            {
                Hrmodel hrmodel = new Hrmodel();
                userRepository = new UserRepository();
                User a = userRepository.Get_User(user.username, user.password);
                if (a != null)
                {
                    Session["User"] = a;
                    UserAuthentication uauth = new UserAuthentication() { user = a };
                    hrmodel.user_auth.Attach(uauth);
                    hrmodel.Entry(uauth).State = System.Data.Entity.EntityState.Added;
                    hrmodel.SaveChanges();
                    a.lastlogindate = DateTime.Now;
                    userRepository.Modify_User(a);
                }
                return Json(a, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content("Failed");
            }
        }

        public ActionResult Forgot()
        {
            if (Session["User"] != null)
            {
                return RedirectToAction("index", "Homepage");
            }
            else
            {
                return View();
            }
        }

        public ActionResult PasswordReset(String Email)
        {

            userRepository = new UserRepository();

            PasswordResetRepository passwordResetRepository = new PasswordResetRepository();
            User user = userRepository.Get_User(Email);
            if (user != null)
            {
                TokenGenerator tokenGenerator = new TokenGenerator();
                PasswordReset passwordReset = new PasswordReset();
                passwordReset.user = user;
                passwordReset.Token = tokenGenerator.Generate_Token();
                passwordReset.created_at = DateTime.Now;
                if (passwordResetRepository.Add_PasswordReset_Request(passwordReset))
                {
                    Email_template template = new Email_template();
                    template.Subject = "PASSWORD RESET REQUEST";
                    template.Reciever_Email = Email;
                    template.SendersName = "Donotreply";
                    template.Senders_Email = "sanketkambli09@gmail.com";
                    template.Body = "";
                    template.Body = "<p>Hello</p><br>" +
                        "We Recieved Your request to reset password<br>" +
                        "click on following link to reset password :<a href='http://localhost:57283/Home/forgotpassword?id=" + passwordReset.Token + "'>http://localhost:57283/Home/forgotpassword?id=" + passwordReset.Token + "</a>";
                    EmailSender email = new EmailSender();
                    if (email.Send_Mail(template))
                    {
                        return Content("Successfully Accepted the request");
                    }
                    else
                    {
                        passwordResetRepository.Remove_PasswordReset_Request(passwordReset);
                        return Content("Something Went Wrong try Again");
                    }
                }
                else
                {
                    return Content("Something Went Wrong try Again");
                }
            }
            else
            {
                return Content("Email Id is not Exist");
            }


        }
        public ActionResult Forgotpassword(String id)
        {
            if (Session["User"] == null)
            {
                TokenGenerator tokenGenerator = new TokenGenerator();
                Hrmodel hrmodel = new Hrmodel();
                List<PasswordReset> passwordResets = hrmodel.passwordResets.ToList();
                PasswordReset p = (from PasswordReset in passwordResets where PasswordReset.Token == id select PasswordReset).FirstOrDefault();
                if (p != null)
                {
                    Session["passwordresetrequest"] = p;
                    return View();
                }
                else
                {
                    return RedirectToAction("index", "Homepage");
                }
            }
            else
            {
                return RedirectToAction("index", "Homepage");
            }
        }

        public ActionResult Register()
        {
            if (Session["User"] == null)
            {
                return View("Register");
            }
            else
            {
                return RedirectToAction("index", "Homepage");
            }
        }
        public ActionResult AddnewUser(User user)
        {
            String message = "Something Went Wrong";
            UserRepository repository = new UserRepository();
            if (repository.Get_User(user.email) != null)
            {
                message = "Email id ";
                if (repository.GetUserByUsername(user.username) != null)
                {
                    message += "& Username ";
                }
                message += "is already exist";
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            else if (repository.GetUserByUsername(user.username) != null)
            {
                message = "Username is already exist";
                return Json(message, JsonRequestBehavior.AllowGet);

            }
            else
            {
                Boolean result = repository.Add_User(user);
                if (result)
                {
                    message = "Successfull";
                }
                return Json(message, JsonRequestBehavior.AllowGet);

            }

        }
        public ActionResult updatepassword(String password)
        {
            if (Session["passwordresetrequest"] != null)
            {
                try
                {
                    PasswordResetRepository repository = new PasswordResetRepository();
                    PasswordReset passwordReset = (PasswordReset)Session["passwordresetrequest"];
                    UserRepository user_repo = new UserRepository();
                    passwordReset.user.Newpassword = password;
                    user_repo.Modify_User(passwordReset.user);
                    repository.Remove_PasswordReset_Request(new Hrmodel().passwordResets.Find(passwordReset.id));
                    Session["passwordresetrequest"] = null;
                    return Content("updated");
                }
                catch (Exception ex)
                {
                    return Content("Something Went Wrong");
                }
            }
            else
            {
                return Content("Something Went Wrong");
            }
        }
    }
}