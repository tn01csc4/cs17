﻿using REST_CONTROLLER.Models;
using RESTC.HelperDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [ApiAuthenticationFilter]
    public class ValuesController : ApiController
    {
        // GET: api/Values
        public IHttpActionResult Get()
        {
            try
            {
                int table_id = Convert.ToInt32(this.Request.Headers.GetValues("table_id").FirstOrDefault());
                Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                return Ok(dml.Getvalues(table_id));
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // GET: api/Values/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                int table_id = Convert.ToInt32(this.Request.Headers.GetValues("table_id").FirstOrDefault());
                Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                return Ok(dml.Getvalues(table_id, id));
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // POST: api/Values
        public IHttpActionResult Post([FromBody]Dictionary<String, Object> Body)
        {
            try
            {
                int table_id = Convert.ToInt32(this.Request.Headers.GetValues("table_id").FirstOrDefault());
                Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                String result = dml.insertRecord(table_id, Body);
                if (result == "Inserted")
                {
                    return Content(HttpStatusCode.Created, "Successfully Created");
                }
                else
                {
                    return Content(HttpStatusCode.NotImplemented, "Failed to Insert");
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // PUT: api/Values/5
        public IHttpActionResult Put(int id, [FromBody]Dictionary<String, Object> Body)
        {
            try
            {
                int table_id = Convert.ToInt32(this.Request.Headers.GetValues("table_id").FirstOrDefault());
                Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                String Result = dml.UpdateRecord(table_id, id, Body);
                if (Result == "updated")
                {
                    return Ok();
                }
                else
                {
                    return Content(HttpStatusCode.NotImplemented, "Failed to update");
                }

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // DELETE: api/Values/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                int table_id = Convert.ToInt32(this.Request.Headers.GetValues("table_id").FirstOrDefault());
                Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                String Result = dml.deleteRecord(table_id, id);
                if (Result == "deleted")
                {
                    return Ok();
                }
                else
                {
                    return Content(HttpStatusCode.NotImplemented, "Failed to delete");
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
