﻿using REST_CONTROLLER.Models;
using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
using RESTC.HelperDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{

    [UserAuthenticationFilter]
    public class DataController : ApiController
    {
        Db_tableRepository tableRepository = new Db_tableRepository();
        // GET: api/Data/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Db_tables tab = tableRepository.Get_Table_Record(id);
                if (tab.user.id == user_id)
                {
                    Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                    return Ok(dml.Getvalues(id));
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // POST: api/Data
        public IHttpActionResult Post([FromUri]int table_id, [FromBody]Dictionary<String, Object> Body)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Db_tables tab = tableRepository.Get_Table_Record(table_id);
                if (tab.user.id == user_id)
                {
                    Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                    String result = dml.insertRecord(table_id, Body);
                    if (result == "Inserted")
                    {
                        return Ok();
                    }
                    else
                    {
                        return Content(HttpStatusCode.NotImplemented, result);
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // PUT: api/Data/5
        public IHttpActionResult Put([FromUri]int id, [FromBody] Dictionary<String, Object> Body)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Db_tables tab = tableRepository.Get_Table_Record(id);
                if (tab.user.id == user_id)
                {
                    Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                    String result = dml.deleteRecord(id, Body);
                    if (result == "deleted")
                    {
                        return Content(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        return Content(HttpStatusCode.NotImplemented, result);
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, ex.Message);
            }
        }

    }
}
