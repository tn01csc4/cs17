﻿using REST_CONTROLLER.Models;
using RESTC.Email_templateDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [UserAuthenticationFilter]
    public class EmailtemplateController : ApiController
    {
        private Emailrepository emailrepository = new Emailrepository();
        // GET: api/Emailtemplate
        public HttpResponseMessage Get()
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                List<Email_template> _Templates = emailrepository.Get_All_Email_Templates();
                var item = (from Email_template in _Templates where Email_template.User.id == user_id select Email_template).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, item);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST: api/Emailtemplate
        public HttpResponseMessage Post(Email_template _Template)
        {
            if (emailrepository.Add_Email_Template(_Template))
            {
                return Request.CreateResponse(HttpStatusCode.Created, "Successfully Added");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Failed to Add");

            }
        }

        // PUT: api/Emailtemplate/5
        public HttpResponseMessage Put(Email_template email_Template)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                if (email_Template.User.id == user_id)
                {
                    if (emailrepository.Modify_Email_Template(email_Template))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Successfully updated");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Failed to update");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Emailtemplate/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Email_template _Template = emailrepository.Get_Email_Template(id);
                if (_Template.User.id == user_id)
                {
                    if (emailrepository.Remove_Email_Template(_Template))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Successfully deleted");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Failed to remove");

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}
