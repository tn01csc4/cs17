﻿using REST_CONTROLLER.Models;
using RESTC.SystemCorsDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [UserAuthenticationFilter]
    public class CorsdetailController : ApiController
    {
        private Corsrepository corsrepository = new Corsrepository();
        // GET: api/Corsdetail
        public HttpResponseMessage Get()
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                List<Corsdetails> corsdetails = corsrepository.Get_All_Cors_config_details();
                var item = (from Corsdetails in corsdetails where Corsdetails.user.id == user_id select Corsdetails).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, item);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST: api/Corsdetail

        public HttpResponseMessage Post(Corsdetails corsdetails)
        {
            try
            {
                if (corsrepository.Add_Cors_config(corsdetails))
                {
                    return Request.CreateResponse(HttpStatusCode.Created, "Successfully Added");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Failed to Add");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Corsdetail/5
        public HttpResponseMessage Put(Corsdetails corsdetails)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                if (corsdetails.user.id == user_id)
                {
                    if (corsrepository.Modify_Cors_config(corsdetails))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Successfully updated");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotModified, "Failed to Modify");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Corsdetail/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Corsdetails corsdetails = corsrepository.Get_Cors_config_details(id);
                if (corsdetails.user.id == user_id)
                {
                    if (corsrepository.Remove_Cors_config(corsdetails))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Successfully deleted");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Failed to delete");

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}
