﻿using REST_CONTROLLER.Models;
using RESTC.RoleDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [UserAuthenticationFilter]
    public class RoleController : ApiController
    {
        private Rolerepository rolerepository = new Rolerepository();
        // GET: api/Role
        public HttpResponseMessage Get()
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                List<Role> roles = rolerepository.Get_All_Roles();
                var items = (from Role in roles where Role.user.id == user_id select Role).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, items);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // GET: api/Role/5
        public HttpResponseMessage Get(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Role role = rolerepository.Get_Role(id);

                if (role != null)
                {
                    if (role.user.id == user_id)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, role);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST: api/Role
        public HttpResponseMessage Post(Role role)
        {
            try
            {
                if (rolerepository.Set_Role(role))
                {
                    return Request.CreateResponse(HttpStatusCode.Created, "Successfully Inserted");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotImplemented, "failed to insert");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Role/5
        public HttpResponseMessage Put(Role role)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                if (role.user.id == user_id)
                {
                    if (rolerepository.Modify_Role(role))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Successfully Modified");

                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotImplemented, "failed to insert");

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/Role/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Role role = rolerepository.Get_Role(id);
                if (role != null && role.user.id == user_id)
                {
                    if (rolerepository.Remove_Role(rolerepository.Get_Role(id)))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Successfully removed");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Failed to Remove");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}
