﻿using REST_CONTROLLER.Models;
using RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory;
using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
using RESTC.ServiceDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [UserAuthenticationFilter]
    public class TableController : ApiController
    {
        ServiceRepository serviceRepository = new ServiceRepository();

        // GET: api/Table/5

        public IHttpActionResult Get(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Service service = serviceRepository.Get_Service_details(id);
                if (service.User.id == user_id)
                {
                    Db_tableRepository repository = new Db_tableRepository();
                    return Ok(repository.Get_tables_(id));
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Db_tableRepository tableRepository = new Db_tableRepository();
                Db_tables tab = tableRepository.Get_Table_Record(id);
                if (tab.user.id == user_id)
                {
                    if (tableRepository.Remove_Db_Table_Record(tableRepository.Get_Table_Record(id)))
                    {
                        return Ok();
                    }
                    return NotFound();
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        public IHttpActionResult Post(List<Table_fields_constraints> Table_Fields)
        {
            try
            {
                DB_Table_fieldRepository fieldRepository = new DB_Table_fieldRepository();
                if (fieldRepository.Add_Table_Field(Table_Fields))
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        public IHttpActionResult Put([FromBody] Db_tables table)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                if (table.user.id == user_id)
                {
                    Db_tableRepository repo = new Db_tableRepository();
                    if (repo.Modify_Db_Table_Record(table))
                    {
                        return Ok();
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
