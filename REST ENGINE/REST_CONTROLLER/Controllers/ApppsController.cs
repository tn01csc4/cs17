﻿using REST_CONTROLLER.Models;
using RESTC.AppDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [UserAuthenticationFilter]
    public class ApppsController : ApiController
    {
        private Apprepository apprepository = new Apprepository();
        // GET: api/Appps

        public HttpResponseMessage Get()
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                List<App> apps = apprepository.Get_All_Apps_details();
                var items = (from App in apps where App.Createdby.id == user_id select App).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, items);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }

        // GET: api/Appps/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Appps
        public HttpResponseMessage Post(App app)
        {
            try
            {
                if (apprepository.Set_App_details(app))
                {
                    return Request.CreateResponse(HttpStatusCode.Created, "Successfully Inserted");

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotImplemented, "failed to insert");

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/Appps/5
        public HttpResponseMessage Put(App app)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                if (app.Createdby.id == user_id)
                {

                    if (apprepository.Modify_app_details(app))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Successfully Modified");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Failed to Modify");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "You Don't have access to Modify this app");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }

        // DELETE: api/Appps/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                App app = apprepository.Get_App_details(id);
                if (app != null && app.Createdby.id == user_id)
                {
                    if (apprepository.Remove_app_details(apprepository.Get_App_details(id)))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Successfully Removed");

                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, "Failed to Remove");

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, "You Can't Delete this App");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            }
        }
    }
}
