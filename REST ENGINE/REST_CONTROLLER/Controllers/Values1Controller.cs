﻿using REST_CONTROLLER.Models;
using RESTC.HelperDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [ApiAuthenticationFilter]
    public class Values1Controller : ApiController
    {
        // GET: api/Values1
        public IHttpActionResult Get(String condition)
        {
            try
            {
                int table_id = Convert.ToInt32(this.Request.Headers.GetValues("table_id").FirstOrDefault());
                Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                return Ok(dml.Getvalues(table_id, condition));
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // GET: api/Values1/5
        public IHttpActionResult Get(String AttributeName, String val)
        {
            try
            {
                int table_id = Convert.ToInt32(this.Request.Headers.GetValues("table_id").FirstOrDefault());
                Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                return Ok(dml.Getvalues(table_id, AttributeName, val));
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }



        // PUT: api/Values1/5
        public IHttpActionResult Put(String AttributeName, String val, [FromBody]Dictionary<String, Object> Body)
        {
            try
            {
                int table_id = Convert.ToInt32(this.Request.Headers.GetValues("table_id").FirstOrDefault());
                Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                String result = dml.UpdateRecord(table_id, AttributeName, val, Body);
                if (result == "updated")
                {
                    return Content(HttpStatusCode.OK, "Successfully Updated");
                }
                else
                {
                    return Content(HttpStatusCode.NotImplemented, result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // DELETE: api/Values1/5
        public IHttpActionResult Delete(String AttributeName, String val)
        {
            try
            {
                int table_id = Convert.ToInt32(this.Request.Headers.GetValues("table_id").FirstOrDefault());
                Dynamic_DML_Commands dml = new Dynamic_DML_Commands();
                String result = dml.deleteRecord(table_id, AttributeName, val);
                if (result == "deleted")
                {
                    return Content(HttpStatusCode.OK, "Successfully deleted");
                }
                else
                {
                    return Content(HttpStatusCode.NotImplemented, result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}