﻿using REST_CONTROLLER.Models;
using RESTC.ServiceDirectory;
using RESTC.ServiceRoleDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [UserAuthenticationFilter]
    public class ServiceController : ApiController
    {
        // GET: api/Service
        public IHttpActionResult Get()
        {
            ServiceRepository repository = new ServiceRepository();
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                List<ServiceRole> serviceroles = repository.Get_All_ServiceRole();
                var item = (from ServiceRole in serviceroles where ServiceRole.Service.User.id == user_id select ServiceRole).ToList();
                return Ok(item);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }


        // POST: api/Service
        public IHttpActionResult Post(ServiceRole ServiceRole)
        {
            try
            {
                ServiceRepository serviceRepository = new ServiceRepository();
                if (serviceRepository.Set_Service_details(ServiceRole))
                {
                    return Ok("Successfully Added");
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex.InnerException);
            }
        }

        // PUT: api/Service/5
        public IHttpActionResult Put(ServiceRole ServiceRole)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                ServiceRepository serviceRepository = new ServiceRepository();
                if (ServiceRole.Service.User.id == user_id)
                {
                    if (serviceRepository.Modify_Service_details(ServiceRole))
                    {
                        return Ok("Successfully Modified");
                    }
                    return NotFound();
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex.InnerException);
            }
        }

        // DELETE: api/Service/5
        public IHttpActionResult Delete(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                ServiceRepository serviceRepository = new ServiceRepository();
                ServiceRole sv = serviceRepository.Get_ServiceRole(id);
                if (sv.Service.User.id == user_id)
                {
                    if (serviceRepository.Remove_Service_details(id))
                    {
                        return Ok("Successfully deleted");
                    }
                    return NotFound();
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex.InnerException);
            }
        }
    }
}
