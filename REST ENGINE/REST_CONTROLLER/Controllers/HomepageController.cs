﻿using RESTC;
using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
using RESTC.UserDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace REST_CONTROLLER.Controllers
{
    public class HomepageController : Controller
    {
        // GET: Homepage
        public ActionResult Index()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("index", "Home");

            }

        }
        public ActionResult Logout()
        {
            try
            {
                Hrmodel hrmodel = new Hrmodel();
                User user = (User)Session["User"];
                Session.Remove("User");
                List<UserAuthentication> uauths = hrmodel.user_auth.ToList();
                UserAuthentication u = (from UserAuthentication in uauths where UserAuthentication.user.id == user.id select UserAuthentication).FirstOrDefault();
                hrmodel.user_auth.Remove(u);
                hrmodel.SaveChanges();

                return RedirectToAction("index", "Home");
            }
            catch (Exception ex)
            {
                Session.Remove("User");
                return RedirectToAction("index", "Home");
            }
        }
        public new ActionResult Profile()
        {
            if (Session["User"] != null)
            {
                User u = (User)Session["User"];
                return View(u);
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        [HttpPost]
        public new ActionResult Profile(User user)
        {
            UserRepository repository = new UserRepository();
            if (ModelState.IsValid)
            {
                repository.Modify_User(user);
            }
            return View(user);
        }

        public ActionResult Users()
        {
            if (Session["User"] != null)
            {
                User user = (User)Session["User"];
                if (user.is_sysadmin)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("index");
                }
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        public ActionResult App()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        public ActionResult Role()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        public ActionResult Service()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        public ActionResult Apidocs()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        public ActionResult Data()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        public ActionResult config()
        {
            if (Session["User"] != null)
            {
                return View("cors");
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        public ActionResult schema()
        {
            if (Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("index", "Home");
            }
        }
        public ActionResult getDatatypes()
        {
            if (Session["User"] != null)
            {
                return Json(Datatype_provider.getAllDatatypes(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content("");
            }
        }
        public ActionResult getPKtables(int service_id)
        {
            if (Session["User"] != null)
            {
                Db_tableRepository repo = new Db_tableRepository();
                return Json(repo.get_Tables_for_PK(service_id), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content("");
            }
        }

    }
}