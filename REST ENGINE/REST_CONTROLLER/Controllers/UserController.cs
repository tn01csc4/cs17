﻿using REST_CONTROLLER.Models;
using RESTC.UserDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [UserAuthenticationFilter]
    public class UserController : ApiController
    {
        private UserRepository userRepository = new UserRepository();
        // GET: api/User

        public HttpResponseMessage Get()
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                User u = userRepository.Get_User(user_id);
                if (u != null && u.is_sysadmin == true)
                {
                    List<User> users = userRepository.GetAllUsers();
                    return Request.CreateResponse(HttpStatusCode.OK, users);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/User/5        
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                User u1 = userRepository.Get_User(user_id);
                if (u1 != null && u1.is_sysadmin)
                {
                    User u = userRepository.Get_User(id);
                    if (u == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, "User Not found");
                    }
                    if (userRepository.Remove_User(u))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "User Succesfully deleted");
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, "Something Went wrong");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}
