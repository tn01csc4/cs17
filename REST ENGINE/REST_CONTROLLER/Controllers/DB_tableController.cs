﻿using REST_CONTROLLER.Models;
using RESTC.DatabaseInfoDirectory.DB_TableFieldInfoDirectory;
using RESTC.DatabaseInfoDirectory.DB_TableInfoDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST_CONTROLLER.Controllers
{
    [UserAuthenticationFilter]
    public class DB_tableController : ApiController
    {

        // GET: api/DB_table/5

        public IHttpActionResult Get(int id)
        {
            Db_tableRepository tableRepository = new Db_tableRepository();
            DB_Table_fieldRepository _FieldRepository = new DB_Table_fieldRepository();
            try
            {
                Db_tables table = tableRepository.Get_Table_Record(id);
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                if (table.user.id == user_id)
                {
                    return Ok(_FieldRepository.Get_All_Table_Field_details(id));
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // POST: api/DB_table

        public IHttpActionResult Post(Table_fields_constraints table_Field)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Db_tableRepository tableRepository = new Db_tableRepository();
                Db_tables table = tableRepository.Get_Table_Record(table_Field.Field.Table_id);
                if (table.user.id == user_id)
                {
                    DB_Table_fieldRepository fieldRepository = new DB_Table_fieldRepository();
                    String result = fieldRepository.AddColumn(table_Field);
                    if (result == "Column Added")
                    {
                        return Content(HttpStatusCode.Created, result);
                    }
                    else
                    {
                        return Content(HttpStatusCode.NotImplemented, result);
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // PUT: api/DB_table/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/DB_table/5

        public IHttpActionResult Delete(int id)
        {
            try
            {
                int user_id = Convert.ToInt32(this.Request.Headers.GetValues("user_id").FirstOrDefault());
                Db_tableRepository tableRepository = new Db_tableRepository();
                DB_Table_fieldRepository repository = new DB_Table_fieldRepository();
                Table_fields_constraints field = repository.Get_Table_Field_details(id);
                Db_tables tab = tableRepository.Get_Table_Record(field.Field.Table_id);
                if (tab.user.id == user_id)
                {
                    String result = repository.dropColumn(id);
                    if (result == "Column Droped")
                    {
                        return Ok(result);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
