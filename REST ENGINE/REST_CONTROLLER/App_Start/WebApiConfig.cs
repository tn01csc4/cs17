﻿using RESTC.SystemCorsDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace REST_CONTROLLER.App_Start
{
    public static class WebApiConfig
    {

        public static void Register(HttpConfiguration config)
        {
            Corsrepository corsrepository = new Corsrepository();
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();


            config.Routes.MapHttpRoute(
                name: "UserApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "New1Api",
                routeTemplate: "api/_table/Values/{Table}/where/{AttributeName}/{val}",
                defaults: new { Table = RouteParameter.Optional, AttributeName = RouteParameter.Optional, val = RouteParameter.Optional, controller = "values1" });
            config.Routes.MapHttpRoute(
                name: "NewApi",
                routeTemplate: "api/_table/Values/{Table}/{id}",
                defaults: new { Table = RouteParameter.Optional, id = RouteParameter.Optional, controller = "values" });

            List<Corsdetails> corsdetails = corsrepository.Get_All_Cors_config_details();
            foreach (Corsdetails cor in corsdetails)
            {
                EnableCorsAttribute cors = new EnableCorsAttribute(cor.Origin, cor.Header, cor.getMethods(), cor.ExposedHeader);
                config.EnableCors(cors);
            }
        }
    }
}