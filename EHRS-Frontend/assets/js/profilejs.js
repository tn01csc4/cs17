myapp.controller('mycntrl',function ($scope,$http,$window) {
    $scope.load = function () {
        $scope.user = sessionStorage.user;
        if (sessionStorage.user == 'null' || $scope.user == undefined) {
            $window.open('login.php', '_self');
        }
        else {

            $scope.User = JSON.parse(sessionStorage.user);
        }
    }
});