myapp.controller('mycntrl',function ($scope,$http,$window) {
    $scope.load=function () {
        $scope.user = sessionStorage.user;
        if (sessionStorage.user == 'null' || $scope.user == undefined) {
            $window.open('login.php', '_self');
        }
        else
            {
                $scope.User = JSON.parse(sessionStorage.user);
                var myParam = location.search.split('d_id=')[1];
                if(myParam==undefined)
                {
                    $window.history.back();
                }
                else
                    {

                        $http({
                                  method: 'GET',
                                  url: 'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/doctors/' + myParam,
                        headers:
                        {
                            "Content-Type": "application/json",
                            "api_key": key
                        }

                    }).then(function (response) {
                            if(response.status==200)
                            {
                                $scope.Doctor=response.data;
                                if(angular.equals($scope.Doctor,{}))
                                {
                                    $window.history.back();
                                }
                                else
                                    {
                                        $http({
                                            method:'GET',
                                            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Country',
                                            headers:{
                                                "Content-Type": "application/json",
                                                "api_key": key
                                            }
                                        }).then(function (response) {
                                            if(response.status==200)
                                            {
                                                $scope.Countries=response.data;
                                            }
                                            $http(
                                                {
                                                    method:'GET',
                                                    url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Departments',
                                                    headers:{
                                                        "Content-Type": "application/json",
                                                        "api_key": key
                                                    }

                                                }).
                                            then(function(resp) {
                                                if (resp.status == 200) {
                                                    $scope.Departments = resp.data;
                                                }
                                                $http({
                                                    method:'GET',
                                                    url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/State/WHERE/Country_id/'+$scope.Doctor.Country_id,
                                                    headers:{
                                                        "Content-Type": "application/json",
                                                        "api_key": key
                                                    }
                                                }).then(function (response) {
                                                    if(response.status==200)
                                                    {
                                                        $scope.States=response.data;
                                                    }
                                                }),function (error) {
                                                    console.log(error);
                                                }
                                            }),function (error) {
                                                console.log(error);

                                            }
                                        }),function (error) {
                                            console.log(error);
                                        }
                                    }
                            }
                        })

                    }
            }
    }
    $scope.getcity=function () {
        $http({
            method:'GET',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/State/WHERE/Country_id/'+$scope.Doctor.Country_id,
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            }
        }).then(function (response) {
            if(response.status==200)
            {
                $scope.States=response.data;
            }
        }),function (error) {
            console.log(error);
        }
    }
    $scope.updatedoctor=function () {

        $scope.NewDoctor={};
        $scope.NewDoctor.FirstName=$scope.Doctor.FirstName;
        $scope.NewDoctor.LastName=$scope.Doctor.LastName;
        $scope.NewDoctor.Email=$scope.Doctor.Email;
        $scope.NewDoctor.Degree=$scope.Doctor.Degree;
        $scope.NewDoctor.Gender=$scope.Doctor.Gender;
        $scope.NewDoctor.Address=$scope.Doctor.Address;
        $scope.NewDoctor.Country_id=$scope.Doctor.Country_id;
        $scope.NewDoctor.State_id=$scope.Doctor.State_id;
        $scope.NewDoctor.City=$scope.Doctor.City;
        $scope.NewDoctor.Postal_code=$scope.Doctor.Postal_code;
        $scope.NewDoctor.Mobile=$scope.Doctor.Mobile;
        $scope.NewDoctor.Department_id=$scope.Doctor.Department_id;
        $scope.NewDoctor.Biography=$scope.Doctor.Biography;
        $scope.NewDoctor.Status=$scope.Doctor.Status;
        $http({
            method:'PUT',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Doctors/'+$scope.Doctor.Id,
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            },
            data:angular.toJson($scope.NewDoctor)
        }).then(function (response) {
            if(response.status==200)
            {
                alert("Successfully updated");
                $window.history.back();
            }
        })
    }
})