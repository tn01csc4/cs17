myapp.controller('mycntrl',function ($scope,$http,$window) {
    $scope.load=function () {
        $scope.user = sessionStorage.user;
        if (sessionStorage.user == 'null' || $scope.user == undefined) {
            $window.open('login.php', '_self');
        }
        else
        {
            $scope.User = JSON.parse(sessionStorage.user);
            var myParam = location.search.split('p_id=')[1];
            if(myParam==undefined)
            {
                $window.history.back();
            }
            else
            {

                $http({
                    method: 'GET',
                    url: 'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/patients/' + myParam,
                    headers:
                        {
                            "Content-Type": "application/json",
                            "api_key": key
                        }

                }).then(function (response) {
                    if(response.status==200)
                    {
                        $scope.Patient=response.data;
                        if(angular.equals($scope.Patient,{}))
                        {
                            $window.history.back();
                        }
                        else
                        {
                            $http({
                                method:'GET',
                                url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Country',
                                headers:{
                                    "Content-Type": "application/json",
                                    "api_key": key
                                }
                            }).then(function (response) {
                                if(response.status==200)
                                {
                                    $scope.Countries=response.data;
                                }
                                    $http({
                                        method:'GET',
                                        url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/State/WHERE/Country_id/'+$scope.Patient.Country_id,
                                        headers:{
                                            "Content-Type": "application/json",
                                            "api_key": key
                                        }
                                    }).
                                    then(function (response) {
                                        if(response.status==200)
                                        {
                                            $scope.States=response.data;
                                        }
                                    }),
                                        function (error) {
                                        console.log(error);
                                    }
                                }),function (error) {
                                    console.log(error);

                                }

                        }
                    }
                })

            }
        }
    }
    $scope.getcity=function () {
        $http({
            method:'GET',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/State/WHERE/Country_id/'+$scope.Patient.Country_id,
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            }
        }).then(function (response) {
            if(response.status==200)
            {
                $scope.States=response.data;
            }
        }),function (error) {
            console.log(error);
        }
    }
    $scope.updatePatient=function () {

        $scope.NewPatient={};
        $scope.NewPatient.FirstName=$scope.Patient.FirstName;
        $scope.NewPatient.LastName=$scope.Patient.LastName;
        $scope.NewPatient.Email=$scope.Patient.Email;
        $scope.NewPatient.Age=$scope.Patient.Degree;
        $scope.NewPatient.Gender=$scope.Patient.Gender;
        $scope.NewPatient.Address=$scope.Patient.Address;
        $scope.NewPatient.Country_id=$scope.Patient.Country_id;
        $scope.NewPatient.State_id=$scope.Patient.State_id;
        $scope.NewPatient.City=$scope.Patient.City;
        $scope.NewPatient.Postalcode=$scope.Patient.Postalcode;
        $scope.NewPatient.Mobile=$scope.Patient.Mobile;
        $scope.NewPatient.Status=$scope.Patient.Status;
        $scope.NewPatient.Diseases=$scope.Patient.Diseases;
        $http({
            method:'PUT',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Patients/'+$scope.Patient.Id,
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            },
            data:angular.toJson($scope.NewPatient)
        }).then(function (response) {
            if(response.status==200)
            {
                alert("Successfully updated");
                $window.history.back();
            }
        })
    }
})