myapp.controller('mycntrl',function ($scope,$http,$window)
{
    $scope.load=function () {
        $scope.user = sessionStorage.user;
        if (sessionStorage.user == 'null' || $scope.user == undefined) {
            $window.open('login.php', '_self');
        }
        else
        {
            $scope.User = JSON.parse(sessionStorage.user);
            $http({
                method:'GET',
                url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Country',
                headers:{
                    "Content-Type": "application/json",
                    "api_key": key
                }
            }).then(function (response) {
                if (response.status == 200) {
                    $scope.Countries = response.data;
                }
            })
        }
    }
    $scope.AddPatient=function () {

        $scope.Patient.created_by=$scope.User.Id;
        console.log(angular.toJson($scope.Patient));
        $http({
            method:'POST',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/patients',
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            },
            data:angular.toJson($scope.Patient)
        }).then(function (response) {
            if(response.status==201)
            {
                alert("Successfully Created");
                $window.history.back();
            }
        }),function (error) {
            console.log(error);
        }

    }
    $scope.getcity=function () {
        $http({
            method:'GET',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/State/WHERE/Country_id/'+$scope.Patient.Country_id,
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            }
        }).then(function (response) {
            if(response.status==200)
            {
                $scope.States=response.data;
            }
        }),function (error) {
            console.log(error);
        }
    }
});