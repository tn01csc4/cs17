myapp.controller('mycntrl',function ($scope,$http,$window) {
    $scope.load=function () {
        $scope.user = sessionStorage.user;
        if (sessionStorage.user == 'null' || $scope.user == undefined) {
            $window.open('login.php', '_self');
        }
        else
            {
            $scope.User = JSON.parse(sessionStorage.user);
            $http({
                method:'GET',
                url: 'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Appointments/',
                headers:
                    {
                        "Content-Type": "application/json",
                        "api_key": key
                    }
            }).then(function (response) {
                if(response.status==200)
                {
                    $scope.Appointments=response.data;
                }
            }),function (error) {
               console.log(error);
            }
            }
    }
    $scope.deleteappointment=function (id) {
        $http(
            {
                method:'DELETE',
                url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Appointments/'+id,
                headers:{
                    "Content-Type": "application/json",
                    "api_key": key
                }

            }).
        then(function(response) {
            if (response.status == 200) {
                alert("Successfully Deleted");
                $window.location.reload();
            }
        }),function (error) {
            console.log(error);

        }
    }
})