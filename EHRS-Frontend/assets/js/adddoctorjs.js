myapp.controller('mycntrl',function ($scope,$http,$window)
{
    $scope.load=function () {
        $scope.user = sessionStorage.user;
        if (sessionStorage.user == 'null' || $scope.user == undefined) {
            $window.open('login.php', '_self');
        }
        else
            {
            $scope.User = JSON.parse(sessionStorage.user);
            $http({
                method:'GET',
                url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Country',
                headers:{
                    "Content-Type": "application/json",
                    "api_key": key
                }
            }).then(function (response) {
                if(response.status==200)
                {
                    $scope.Countries=response.data;
                }
                $http(
                    {
                        method:'GET',
                        url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Departments',
                        headers:{
                            "Content-Type": "application/json",
                            "api_key": key
                        }

                    }).
                then(function(resp) {
                    if (resp.status == 200) {
                        $scope.Departments = resp.data;
                    }
                }),function (error) {
                    console.log(error);

                }
            }),function (error) {
                console.log(error);
            }
        }
    }
    $scope.Adddoctor=function () {

        $scope.Doctor.Created_by=$scope.User.Id;
        console.log($scope.Doctor);
        $http({
            method:'POST',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/doctors',
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            },
            data:angular.toJson($scope.Doctor)
        }).then(function (response) {
            if(response.status==201)
            {
                alert("Successfully Created");
                $window.history.back();
            }
        }),function (error) {
            console.log(error);
        }

    }
    $scope.getcity=function () {
        $http({
            method:'GET',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/State/WHERE/Country_id/'+$scope.Doctor.Country_id,
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            }
        }).then(function (response) {
            if(response.status==200)
            {
                $scope.States=response.data;
            }
        }),function (error) {
            console.log(error);
        }
    }
});