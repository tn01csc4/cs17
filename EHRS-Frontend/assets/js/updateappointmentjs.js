myapp.controller('myntrl',function ($scope,$http,$window)
{
    $scope.load=function () {
        $scope.user = sessionStorage.user;
        if (sessionStorage.user == 'null' || $scope.user == undefined) {
            $window.open('login.php', '_self');
        }
        else
        {
            $scope.User = JSON.parse(sessionStorage.user);
            var myParam = location.search.split('apt_id=')[1];
            if(myParam==undefined)
            {
                $window.history.back();
            }
            else
                {
                    $http({
                        method: 'GET',
                        url: 'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/appointments/' + myParam,
                        headers:
                            {
                                "Content-Type": "application/json",
                                "api_key": key
                            }

                    }).then(function (response) {
                        if (response.status == 200) {
                            $scope.Appointment = response.data;
                            if (angular.equals($scope.Appointment, {})) {
                                $window.history.back();
                            }
                            else
                                {
                                    $http({
                                        method:'GET',
                                        url: 'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/patients/',
                                        headers:
                                            {
                                                "Content-Type": "application/json",
                                                "api_key": key
                                            }
                                    }).then(function (response) {
                                        if(response.status==200)
                                        {
                                            $scope.Patients=response.data;
                                        }
                                        $http({
                                            method:'GET',
                                            url: 'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/departments/',
                                            headers:
                                                {
                                                    "Content-Type": "application/json",
                                                    "api_key": key
                                                }
                                        }).then(function (response) {
                                            if(response.status==200)
                                            {
                                                $scope.Departments=response.data;
                                            }
                                            $http({
                                                method:'GET',
                                                url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Departments/WHERE/Name/'+$scope.Appointment.DepartmentName,
                                                headers:{
                                                    "Content-Type": "application/json",
                                                    "api_key": key
                                                }
                                            }).
                                            then(function (response) {
                                                if(response.status==200)
                                                {
                                                    $scope.Dept=response.data[0];
                                                }
                                                $http({
                                                    method:'GET',
                                                    url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Doctors/WHERE/Department_id/'+$scope.Dept.Id,
                                                    headers:{
                                                        "Content-Type": "application/json",
                                                        "api_key": key
                                                    }
                                                }).
                                                then(function (response) {
                                                    if(response.status==200)
                                                    {
                                                        $scope.Doctors=response.data;
                                                    }
                                                }),
                                                    function (error) {
                                                        console.log(error);
                                                    }
                                            }),
                                                function (error) {
                                                    console.log(error);
                                                }
                                        }),function (error) {
                                            console.log(error)
                                        }
                                    }),function (error) {
                                        console.log(error)
                                    }
                                }
                        }
                    })
                }
        }
    }
    $scope.getDoctor=function () {
        $http({
            method:'GET',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Departments/WHERE/Name/'+$scope.Appointment.DepartmentName,
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            }
        }).
        then(function (response) {
            if(response.status==200)
            {
                $scope.Dept=response.data[0];
            }
            $http({
                method:'GET',
                url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Doctors/WHERE/Department_id/'+$scope.Dept.Id,
                headers:{
                    "Content-Type": "application/json",
                    "api_key": key
                }
            }).
            then(function (response) {
                if(response.status==200)
                {
                    $scope.Doctors=response.data;
                }
            }),
                function (error) {
                    console.log(error);
                }
        }),
            function (error) {
                console.log(error);
            }



    }
    $scope.updateappointment=function ()
    {
    $scope.newAppointment={};
        $scope.newAppointment.PatientName=$scope.Appointment.PatientName;
        $scope.newAppointment.DepartmentName=$scope.Appointment.DepartmentName;
        $scope.newAppointment.Doctor=$scope.Appointment.Doctor;
        $scope.newAppointment.Date=$scope.Appointment.Date;
        $scope.newAppointment.time=$scope.Appointment.time;
        $scope.newAppointment.PatientEmail=$scope.Appointment.PatientEmail;
        $scope.newAppointment.PatientMobile=$scope.Appointment.PatientMobile;
        $scope.newAppointment.Message=$scope.Appointment.Message;
        $scope.newAppointment.Status=$scope.Appointment.Status;
        $http(
            {
                method:'PUT',
                url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Appointments/'+$scope.Appointment.Id,
                headers:{
                    "Content-Type": "application/json",
                    "api_key": key
                },
                data:angular.toJson($scope.newAppointment)
            }).then(function (response)
        {
        if(response.status==200)
        {
alert("Successfully updated");
$window.history.back();
        }
        }),function (error)
        {
        console.log(error);
        }
    }
})