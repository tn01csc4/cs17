myapp.controller('mycntrl',function ($scope,$http,$window) {
    $scope.load=function () {
        $scope.user=sessionStorage.user;
        if(sessionStorage.user=='null'||$scope.user==undefined)
        {
            $window.open('login.php','_self');
        }
        else
            {
                $scope.User=JSON.parse(sessionStorage.user);
                var myParam = location.search.split('d_id=')[1];
                if(myParam==undefined)
                {
                    $window.history.back();
                }
                else {
                    $http({
                        method: 'GET',
                        url: 'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/departments/' + myParam,
                        headers:
                            {
                                "Content-Type": "application/json",
                                "api_key": key
                            }

                    }).then(function (response) {
                        if (response.status == 200) {
                            if (angular.equals(response.data, {})) {
                                $window.history.back();
                            }
                            else {
                                $scope.Department = response.data;

                            }
                        }
                    }), function (error) {
                        console.log(error)
                    }
                }
            }
    }
    $scope.editdepartment=function () {
        $scope.Department.created_by=$scope.User.Id;
        $scope.NewDepartment={};
        $scope.NewDepartment.Name=$scope.Department.Name;
        $scope.NewDepartment.Status=$scope.Department.Status;
        $scope.NewDepartment.created_by=$scope.Department.created_by;
        $scope.NewDepartment.Description=$scope.Department.Description;
        console.log($scope.NewDepartment);
        $http({
            method:'PUT',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/departments/'+$scope.Department.Id,
            headers:
                {
                    "Content-Type": "application/json",
                    "api_key": key
                },
            data:angular.toJson($scope.NewDepartment)

        }).then(function (response) {
            if(response.status==200)
            {
                alert("Successfully Updated");
                $window.location.href='departments.php';
            }
        }),function (error) {
            console.log(error)
        }
    }
})