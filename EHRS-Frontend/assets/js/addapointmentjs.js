myapp.controller('mycntrl',function ($scope,$http,$window) {
    $scope.load=function () {
        $scope.user = sessionStorage.user;
        if (sessionStorage.user == 'null' || $scope.user == undefined) {
            $window.open('login.php', '_self');
        }
        else
            {
                $scope.User=JSON.parse(sessionStorage.user);
                $http({
                    method:'GET',
                    url: 'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/patients/',
                    headers:
                        {
                            "Content-Type": "application/json",
                            "api_key": key
                        }
                }).then(function (response) {
                    if(response.status==200)
                    {
                        $scope.Patients=response.data;
                    }
                    $http({
                        method:'GET',
                        url: 'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/departments/',
                        headers:
                            {
                                "Content-Type": "application/json",
                                "api_key": key
                            }
                    }).then(function (response) {
                        if(response.status==200)
                        {
                            $scope.Departments=response.data;
                        }
                    }),function (error) {
                        console.log(error)
                    }
                }),function (error) {
                    console.log(error)
                }
            }
    }
    $scope.getDoctor=function () {
        $http({
            method:'GET',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Departments/WHERE/Name/'+$scope.Appointment.DepartmentName,
            headers:{
                "Content-Type": "application/json",
                "api_key": key
            }
        }).
        then(function (response) {
            if(response.status==200)
            {
                $scope.Dept=response.data[0];
            }
            $http({
                method:'GET',
                url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Doctors/WHERE/Department_id/'+$scope.Dept.Id,
                headers:{
                    "Content-Type": "application/json",
                    "api_key": key
                }
            }).
            then(function (response) {
                if(response.status==200)
                {
                    $scope.Doctors=response.data;
                }
            }),
                function (error) {
                    console.log(error);
                }
        }),
            function (error) {
                console.log(error);
            }



    }
    $scope.addappointment=function () {
        $scope.Appointment.createdby=$scope.User.Id;
        $http({
            method:'POST',
            url:'http://restengine.us-east-2.elasticbeanstalk.com/API/_TABLE/VALUES/Appointments',
            headers:
                {
                    "Content-Type": "application/json",
                    "api_key": key
                },
            data:angular.toJson($scope.Appointment)
        }).then(function (response)
        {
         if(response.status==201)
         {
             alert("Successfully Created");
         }
        }),function (error)
        {
        console.log(error);
        }
    }
});