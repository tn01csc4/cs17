<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <title>Preclinic - Medical &amp; Hospital - Bootstrap 4 Admin Template</title>
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="assets/js/appdef.js" type="text/javascript"></script>
    <script src="assets/js/patientsjs.js" type="text/javascript"></script>
</head>

<body ng-app="myapp" ng-controller="mycntrl" ng-init="load()">
<div class="main-wrapper">

<?php include_once "include_top.php"?>
    <div class="page-wrapper" style="min-height: 360px;">
        <div class="content">
            <div class="row">
                <div class="col-sm-4 col-3">
                    <h4 class="page-title">Patients</h4>
                </div>
                <div class="col-sm-8 col-9 text-right m-b-20">
                    <a href="add-patient.php" class="btn btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> Add Patient</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <div class="no-footer"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="col-sm-12"><table class="table table-border table-striped custom-table  mb-0  no-footer" role="grid">
                            <thead>
                            <tr role="row"><th rowspan="1" colspan="1" style="width: 141px;">Name</th><th rowspan="1" colspan="1" style="width: 30px;">Age</th><th tabindex="0" rowspan="1" colspan="1" style="width: 328px;">Address</th><th tabindex="0" rowspan="1" colspan="1" style="width: 85px;">Phone</th><th tabindex="0" rowspan="1" colspan="1" style="width: 193px;">Email</th><th class="text-right sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 52px;">Action</th></tr>
                            </thead>
                            <tbody>
                            <tr role="row" class="{{$index%2==0?'even':'odd'}}" ng-repeat="Patient in Patients">
                                <td class="sorting_1"><img width="28" height="28" src="assets/img/user.jpg" class="rounded-circle m-r-5" alt="">{{Patient.FirstName}} {{Patient.LastName}}</td>
                                <td>{{Patient.Age}}</td>
                                <td>{{Patient.Address}}</td>
                                <td>{{Patient.Mobile}}</td>
                                <td>{{Patient.Email}}</td>
                                <td class="text-right">
                                    <div class="dropdown dropdown-action">
                                        <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="edit-patient.php?p_id={{Patient.Id}}"><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                            <button class="dropdown-item" ng-click="deletepatient(Patient.Id)"><i class="fa fa-trash-o m-r-5"></i> Delete</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                           </tbody>
                        </table></div>
                            </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <div id="delete_patient" class="modal fade delete-modal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <img src="assets/img/sent.png" alt="" width="50" height="46">
                    <h3>Are you sure want to delete this Patient?</h3>
                    <div class="m-t-20"> <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="sidebar-overlay" data-reff=""></div>
<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/select2.min.js"></script>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/js/moment.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/app.js"></script>


</body></html>